#include "VideoSettingDialog.h"
#include "ui_VideoSettingDialog.h"

VideoSettingDialog::VideoSettingDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::VideoSettingDialog)
{
    ui->setupUi(this);
}

VideoSettingDialog::~VideoSettingDialog()
{
    delete ui;
}
