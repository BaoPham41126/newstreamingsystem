/***********************************************************************
 * Module:  IOThreadService.h
 * Author:  BaoPham
 * Modified: Friday, November 24, 2017 1:14:39 AM
 * Purpose: Declaration of the class IOThreadService
 ***********************************************************************/

#if !defined(__IOThread_IOThreadService_h)
#define __IOThread_IOThreadService_h

class IOThread;

#include <QObject>
#include <QList>

#include <Header/Base/Singleton.h>
#include <Header/InputOutput/InputOutputMapperService.h>
#include <Header/Stream/StreamService.h>
#include <Header/Buffer/FrameBufferService.h>
#include <Header/Codec/CodecService.h>
#include <Header/Thread/InputThread.h>
#include <Header/Thread/OutputThread.h>
#include <Header/InputOutput/VideoStreamInfo.h>
#include <Header/InputOutput/AudioStreamInfo.h>
#include "iostream"

class IOThreadService : public QObject
{
    Q_OBJECT

public:
   QList<IOThread*> createIOThread(AVInfo* info);

   ~IOThreadService(){}
   static IOThreadService* instance(){
       return Singleton<IOThreadService>::instance(IOThreadService::createInstance);
   }
protected:
private:
   IOThreadService(QObject* parent = 0) : QObject(parent){}
   static IOThreadService* createInstance(){ return new IOThreadService();}

   InputOutputMapperService* mapperService = InputOutputMapperService::instance();
   StreamService* streamService = StreamService::instance();
   FrameBufferService* bufferService = FrameBufferService::instance();
};

#endif
