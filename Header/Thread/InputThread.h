/***********************************************************************
 * Module:  InputThread.h
 * Author:  BaoPham
 * Modified: Wednesday, November 22, 2017 9:18:47 PM
 * Purpose: Declaration of the class InputThread
 ***********************************************************************/

#if !defined(__IOThread_InputThread_h)
#define __IOThread_InputThread_h

#include <Header/Thread/IOThread.h>
#include <Header/Buffer/FrameBuffer.h>
#include <Header/Stream/InputStream.h>
#include <Header/View/VideoSurface.h>
#include <Header/View/AudioSurface.h>

class ConvertContext{
public:
    struct SwsContext* context;
    int width;
    int height;
    AVPixelFormat pixelFormat;
};

class InputThread : public IOThread
{
    Q_OBJECT

public:
    QList<FrameBuffer*> buffers;
    QList<ConvertContext*> convertContexts;
    InputThread(){}
    ~InputThread(){}
    void run() override;
    void stop() override;
    void init() override;
    VideoSurface* reviewPlayer = NULL;
    AudioSurface* audioReviewPlayer = NULL;
signals:
    void frameChange(AVFrame* frame);
private:

    AVFrame* getReviewFrame(AVFrame* frame);

};

#endif
