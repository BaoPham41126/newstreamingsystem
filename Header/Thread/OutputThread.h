/***********************************************************************
 * Module:  OutputThread.h
 * Author:  BaoPham
 * Modified: Wednesday, November 22, 2017 9:14:49 PM
 * Purpose: Declaration of the class OutputThread
 ***********************************************************************/

#if !defined(__IOThread_OutputThread_h)
#define __IOThread_OutputThread_h

#include <Header/Thread/IOThread.h>
#include <Header/Buffer/FrameBuffer.h>
#include <Header/Stream/OutputStream.h>

class OutputThread : public IOThread
{
    Q_OBJECT

public:
   FrameBuffer* buffer;
   OutputThread(){}
   ~OutputThread(){}
   void run() override;
   void stop() override;
   void init() override;
};

#endif
