/***********************************************************************
 * Module:  IOThread.h
 * Author:  BaoPham
 * Modified: Thursday, November 23, 2017 9:28:28 PM
 * Purpose: Declaration of the class IOThread
 ***********************************************************************/

#if !defined(__IOThread_IOThread_h)
#define __IOThread_IOThread_h
extern "C" {
#include <libavutil/imgutils.h>
#include <libswscale/swscale.h>
}

#include <QObject>
#include <QTime>
#include <QThread>

#include <Header/Stream/AVStream.h>
#include <Header/InputOutput/StreamInfo.h>
#include <Header/Codec/CodecService.h>
#include <iostream>

class IOThread : public QObject {
    Q_OBJECT

protected:
    bool running = true;
public slots:
    virtual void run(){}
    virtual void stop(){}
    virtual void init(){}
signals:
    void finish();
public:
    qint64 startTime;
    StreamInfo* info;
    Stream* stream;
    QThread* thread;
};

#endif
