/***********************************************************************
 * Module:  AudiotInfo.h
 * Author:  BaoPham
 * Modified: Wednesday, November 22, 2017 9:33:54 PM
 * Purpose: Declaration of the class AudiotInfo
 ***********************************************************************/

#if !defined(__InputOutput_AudioInfo_h)
#define __InputOutput_AudioInfo_h
extern "C" {
#include "libavformat/avformat.h"
}
#include <Header/InputOutput/StreamInfo.h>

class AudioStreamInfo : public StreamInfo
{
    Q_OBJECT
public:
   AVSampleFormat sampleFormat;
   int numChanel;
   int sampleRate;

   AudioStreamInfo(QObject* parent = NULL) : StreamInfo(parent){
       this->sampleFormat = AV_SAMPLE_FMT_S32;
       this->sampleRate = 44100;
       this->numChanel = 2;
       this->bitRate = 64000;
       this->queueSize = 100000;
       this->codecID = AV_CODEC_ID_MP3;
       this->streamType = AUDIO;
   }

   void copyFormatOf(StreamInfo* streamInfo) override{
       StreamInfo::copyFormatOf(streamInfo);
       AudioStreamInfo* audioStreamInfo = (AudioStreamInfo*) streamInfo;
       this->sampleFormat = audioStreamInfo->sampleFormat;
       this->numChanel = audioStreamInfo->numChanel;
       this->sampleRate = audioStreamInfo->sampleRate;
   }

   void write(QDataStream &out) const override
   {
       out << QString(QString("audio"));
       StreamInfo::write(out);
       out << quint32(this->sampleFormat) << quint32(this->numChanel) << quint32(this->sampleRate);
   }

   void read(QDataStream &in) override
   {
       StreamInfo::read(in);
       quint32 sampleFormat;
       quint32 numChanel;
       quint32 sampleRate;
       in >> sampleFormat >> numChanel >> sampleRate;
       this->sampleFormat = static_cast<AVSampleFormat>(sampleFormat);
       this->numChanel = numChanel;
       this->sampleRate = sampleRate;
   }
};

#endif
