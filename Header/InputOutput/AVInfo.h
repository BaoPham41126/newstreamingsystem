#ifndef AVINFO_H
#define AVINFO_H
#include <QObject>
#include <QString>
#include <QList>
#include <Header/InputOutput/StreamInfo.h>
#include <Header/InputOutput/VideoStreamInfo.h>
#include <Header/InputOutput/AudioStreamInfo.h>

enum AVType {
    INPUT,
    OUTPUT
};
enum OutputType {
    HDD,
    LIVE,
    YOUTUBE
};

class AVInfo : public QObject
{
    Q_OBJECT

public:
    QString id;
    QString name;
    QString url;
    AVType type;
    bool active = true;

    //Input only
    QString driver;
    //Output only
    QString format;
    OutputType outputType = HDD;

    QList<StreamInfo*> streams;

    void removeStreamById(QString id){
        for (int i = 0; i < streams.size(); i++){
            if (streams.at(i)->id == id){
                streams.removeAt(i);
                break;
            }
        }
    }
    StreamInfo* getStreamById(QString id){
        for (int i = 0; i < streams.size(); i++){
            if (streams.at(i)->id == id){
                return streams.at(i);
            }
        }
        return NULL;
    }

    static bool lessThan(AVInfo* s1, AVInfo* s2){
        return s1->name.toLower() < s2->name.toLower();
    }

    AVInfo* clone(){
        AVInfo* clone = new AVInfo();
        clone->id = this->id;
        clone->name = this->name;
        clone->url = this->url;
        clone->type = this->type;
        clone->active = this->active;
        clone->driver = this->driver;
        clone->format = this->format;
        clone->outputType = this->outputType;
        clone->streams = this->streams;
        return clone;
    }

    friend QDataStream &operator<<(QDataStream &out, const AVInfo &info){
        info.write(out);
        return out;
    }

    virtual void write(QDataStream &out) const {
        out << this->id << this->name << this->url << quint32(this->type) << this->active
            << this->driver << this->format << quint32(this->outputType);
        out << this->streams.size();
        for (int i = 0; i < this->streams.size(); i++){
            out << *(this->streams.at(i));
        }
    }

    friend QDataStream &operator>>(QDataStream &in, AVInfo &info){
        info.read(in);
        return in;
    }

    virtual void read(QDataStream &in){
        quint32 type;
        quint32 outputType;
        in >> this->id >> this->name >> this->url >> type >> this->active
           >> this->driver >> this->format >> outputType;
        this->type = static_cast<AVType>(type);
        this->outputType = static_cast<OutputType>(outputType);
        int num;
        in >> num;
        for (int i = 0; i < num; i++){
            QString streamType;
            in >> streamType;
            if (streamType == "video"){
                VideoStreamInfo* streamInfo = new VideoStreamInfo();
                in >> *streamInfo;
                this->streams.append(streamInfo);
            } else {
                AudioStreamInfo* streamInfo = new AudioStreamInfo();
                in >> *streamInfo;
                this->streams.append(streamInfo);
            }
        }
    }
public slots:
    void changeActiveState(){
        this->active = !this->active;
    }
};

#endif // AVINFO_H
