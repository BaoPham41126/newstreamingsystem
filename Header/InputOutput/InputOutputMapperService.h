/***********************************************************************
 * Module:  InputOutputMapperService.h
 * Author:  BaoPham
 * Modified: Friday, November 24, 2017 1:14:39 AM
 * Purpose: Declaration of the class InputOutputMapperService
 ***********************************************************************/

#if !defined(__InputOutput_InputOutputMapperService_h)
#define __InputOutput_InputOutputMapperService_h
#define DATA_FILE input_output.txt

#include <QObject>
#include <QList>

#include <Header/Base/Singleton.h>
#include <Header/InputOutput/InputService.h>
#include <Header/InputOutput/OutputService.h>
#include <Header/InputOutput/StreamInfo.h>
#include <Header/InputOutput/InputOutput.h>

class InputOutputMapperService : public QObject
{
    Q_OBJECT

public:
   QList<StreamInfo*> getInputOfOutput(QString outputId);
   QList<StreamInfo*> getOutputOfInput(QString inputId);
   InputOutput* bindInputOutput(QString inputId, QString outputId);
   void unbindInputOutput(QString outputId);

   ~InputOutputMapperService(){
       this->saveData();
       inputOutputs.clear();
   }
   static InputOutputMapperService* instance(){
       return Singleton<InputOutputMapperService>::instance(InputOutputMapperService::createInstance);
   }
protected:
private:
   InputOutputMapperService(QObject* parent = 0) : QObject(parent){
       this->loadData();
   }
   static InputOutputMapperService* createInstance(){ return new InputOutputMapperService();}

   InputService* inputService = InputService::instance();
   OutputService* outputService = OutputService::instance();

   QList<InputOutput*> inputOutputs;
   QString dataFileString = "data/input_output.txt";

   void loadData();
   void saveData();
};

#endif
