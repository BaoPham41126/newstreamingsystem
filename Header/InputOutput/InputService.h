/***********************************************************************
 * Module:  InputService.h
 * Author:  BaoPham
 * Modified: Wednesday, November 22, 2017 10:21:17 PM
 * Purpose: Declaration of the class InputService
 ***********************************************************************/

#if !defined(__InputOutput_InputService_h)
#define __InputOutput_InputService_h

#include <QObject>
#include <QList>
#include <QUuid>
#include <QDirIterator>
#include <QFile>
#include <QTextStream>
#include <QScreen>
#include <QGuiApplication>
#include <QProcess>

#include <Header/Base/Singleton.h>
#include <Header/InputOutput/AVInfo.h>
#include <Header/InputOutput/VideoStreamInfo.h>
#include <Header/InputOutput/AudioStreamInfo.h>

class InputService : public QObject
{
    Q_OBJECT

public:
   AVInfo* saveInput(AVInfo* info);
   StreamInfo* getStreamById(QString id);
   QList<AVInfo*> getAllInput();
   void deleteInput(QString id);
   AVInfo* getInputByURL(QString url);
   QList<AVInfo*> getAvaitableVideo(bool reload = false);
   QList<AVInfo*> getAvaitableAudio(bool reload = false);
   void reloadInputFormat();

   ~InputService(){
       this->saveData();
       inputs.clear();
   }
   static InputService* instance(){
       return Singleton<InputService>::instance(InputService::createInstance);
   }
protected:
private:
   InputService(QObject* parent = 0) : QObject(parent){
       this->loadData();
   }
   static InputService* createInstance(){ return new InputService();}

   int getInputIndex(QString id);
   void getInputResolution(QString deviceName, int* width, int* height);

   QList<AVInfo*> inputs;
   QString dataFileString = "data/input.txt";

   void loadData();
   void saveData();
};

#endif
