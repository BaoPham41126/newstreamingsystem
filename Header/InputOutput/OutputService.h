/***********************************************************************
 * Module:  OutputService.h
 * Author:  BaoPham
 * Modified: Wednesday, November 22, 2017 10:21:24 PM
 * Purpose: Declaration of the class OutputService
 ***********************************************************************/

#if !defined(__InputOutput_OutputService_h)
#define __InputOutput_OutputService_h

#include <QObject>
#include <QList>
#include <QUuid>
#include <QFile>

#include <Header/Base/Singleton.h>
#include <Header/Youtube/YoutubeService.h>
#include <Header/Course/CourseService.h>
#include <Header/InputOutput/AVInfo.h>

class OutputService : public QObject
{
    Q_OBJECT

public:
   AVInfo* saveOutput(AVInfo* info);
   StreamInfo* getActiveStreamById(QString id);
   AVInfo* getOutputById(QString id);
   QList<AVInfo*> getAllOutput();
   QList<AVInfo*> getAllActiveOutput();
   void deleteOutput(QString id);

   ~OutputService(){
       this->saveData();
       outputs.clear();
   }
   static OutputService* instance(){
       return Singleton<OutputService>::instance(OutputService::createInstance);
   }
protected:
private:
   OutputService(QObject* parent = 0) : QObject(parent){
       this->loadData();
   }
   static OutputService* createInstance(){ return new OutputService();}

   int getOutputIndex(QString id);

   YoutubeService* youtubeService = YoutubeService::instance();
   CourseService* courseService = CourseService::instance();
   QList<AVInfo*> outputs;
   QString dataFileString = "data/output.txt";

   void loadData();
   void saveData();
};

#endif
