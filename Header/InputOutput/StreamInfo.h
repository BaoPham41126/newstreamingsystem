/***********************************************************************
 * Module:  AVInfo.h
 * Author:  BaoPham
 * Modified: Thursday, November 23, 2017 9:29:41 PM
 * Purpose: Declaration of the class AVInfo
 ***********************************************************************/

#if !defined(__InputOutput_AVInfo_h)
#define __InputOutput_AVInfo_h
extern "C" {
#include "libavcodec/avcodec.h"
}
#include <QObject>
#include <QString>
#include <QDataStream>

enum StreamType {
    VIDEO,
    AUDIO
};

class StreamInfo : public QObject
{
    Q_OBJECT

public:
   QString id;
   QString name;
   int bitRate;
   AVCodecID codecID;
   int queueSize;
   StreamType streamType;

   //Output only
   QString inputId;

   StreamInfo(QObject* parent = NULL) : QObject(parent){}

   virtual void copyFormatOf(StreamInfo* streamInfo){
       this->bitRate = streamInfo->bitRate;
       this->codecID = streamInfo->codecID;
       this->queueSize = streamInfo->queueSize;
   }

   friend QDataStream &operator<<(QDataStream &out, const StreamInfo &info){
       info.write(out);
       return out;
   }

   virtual void write(QDataStream &out) const {
       out << this->id << this->name << quint32(this->bitRate) << quint32(this->codecID) << quint32(this->queueSize);
   }

   friend QDataStream &operator>>(QDataStream &in, StreamInfo &info){
       info.read(in);
       return in;
   }

   virtual void read(QDataStream &in){
       quint32 codecID;
       quint32 bitRate;
       quint32 queueSize;
       in >> this->id >> this->name >> bitRate >> codecID >> queueSize;
       this->bitRate = bitRate;
       this->codecID = static_cast<AVCodecID>(codecID);
       this->queueSize = queueSize;
   }
};

#endif
