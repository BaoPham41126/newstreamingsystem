/***********************************************************************
 * Module:  Videothis->h
 * Author:  BaoPham
 * Modified: Wednesday, November 22, 2017 9:33:36 PM
 * Purpose: Declaration of the class VideoInfo
 ***********************************************************************/

#if !defined(__InputOutput_VideoInfo_h)
#define __InputOutput_VideoInfo_h

#include <Header/InputOutput/StreamInfo.h>

class VideoStreamInfo : public StreamInfo
{
    Q_OBJECT
public:
    int bufSize;
    int maxBufRate;
    int width;
    int height;
    AVPixelFormat pixelFormat;
    int frameRate;
    int GOPSize;
    int baseLine;

    VideoStreamInfo(QObject* parent = NULL) : StreamInfo(parent){
        this->bitRate = 4000000;
        this->GOPSize = 10;
        this->width = 1280;
        this->height = 720;
        this->codecID = AV_CODEC_ID_H264;
        this->queueSize = 300;
        this->maxBufRate = 10;
        this->baseLine = 0;
        this->frameRate = 25;
        this->pixelFormat = AV_PIX_FMT_YUV420P;
        this->streamType = VIDEO;
    }

    void copyFormatOf(StreamInfo* streamInfo) override{
        StreamInfo::copyFormatOf(streamInfo);
        VideoStreamInfo* videoStreamInfo = (VideoStreamInfo*) streamInfo;
        this->bufSize = videoStreamInfo->bufSize;
        this->maxBufRate = videoStreamInfo->maxBufRate;
        this->width = videoStreamInfo->width;
        this->height = videoStreamInfo->height;
        this->pixelFormat = videoStreamInfo->pixelFormat;
        this->frameRate = videoStreamInfo->frameRate;
        this->GOPSize = videoStreamInfo->GOPSize;
        this->baseLine = videoStreamInfo->baseLine;
    }

    void write(QDataStream &out) const override
    {
        out << QString(QString("video"));
        StreamInfo::write(out);
        out << this->bufSize << this->maxBufRate << this->width << this->height << this->pixelFormat
            << this->frameRate << this->GOPSize << this->baseLine;
    }

    void read(QDataStream &in) override
    {
        StreamInfo::read(in);
        int pixelFormat;
        in >> this->bufSize >> this->maxBufRate >> this->width >> this->height >> pixelFormat
           >> this->frameRate >> this->GOPSize >> this->baseLine;
        this->pixelFormat = static_cast<AVPixelFormat>(pixelFormat);
    }
protected:
private:
};

#endif
