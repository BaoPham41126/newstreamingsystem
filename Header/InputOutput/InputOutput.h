/***********************************************************************
 * Module:  InputOutput.h
 * Author:  BaoPham
 * Modified: Wednesday, November 22, 2017 9:47:45 PM
 * Purpose: Declaration of the class InputOutput
 ***********************************************************************/

#if !defined(__InputOutput_InputOutput_h)
#define __InputOutput_InputOutput_h

#include<QObject>
#include<QString>
#include<QUuid>

class InputOutput : public QObject
{
    Q_OBJECT

public:
   QString id;
   QString inputId;
   QString outputId;
   InputOutput(QString inputId, QString outputId){
       QUuid uuid = QUuid::createUuid();
       this->id = uuid.toString();
       this->inputId = inputId;
       this->outputId = outputId;
   }
   InputOutput(){}

   friend QDataStream &operator<<(QDataStream &out, const InputOutput &entry){
       entry.write(out);
       return out;
   }

    void write(QDataStream &out) const {
       out << this->id << this->inputId << this->outputId;
   }

   friend QDataStream &operator>>(QDataStream &in, InputOutput &entry){
       entry.read(in);
       return in;
   }

   virtual void read(QDataStream &in){
       in >> this->id >> this->inputId >> this->outputId;
   }
};

#endif
