#ifndef NEWINPUTDIALOG_H
#define NEWINPUTDIALOG_H

#include "ui_newinputdialog.h"
#include <QDialog>
#include <Header/InputOutput/InputService.h>

namespace Ui {
class NewInputDialog;
}

class NewInputDialog : public QDialog
{
    Q_OBJECT

public:
    explicit NewInputDialog(QWidget *parent = 0);
    ~NewInputDialog();
private slots:
    void on_buttonBox_accepted();
signals:
    void onAccepted(QList<StreamInfo*> streams);
private:
    Ui::NewInputDialog *ui;
    InputService *inputService = InputService::instance();
};

#endif // NEWINPUTDIALOG_H
