#ifndef NEWOUTPUTDIALOG_H
#define NEWOUTPUTDIALOG_H

#include <QDialog>
#include <QLayout>
#include <QSignalMapper>
#include <QMessageBox>
#include <QHBoxLayout>

#include <Header/InputOutput/OutputService.h>
#include <Header/InputOutput/InputOutputMapperService.h>
#include <Header/View/AudioSettingDialog.h>
#include <Header/View/VideoSettingDialog.h>
#include <Header/View/AddInputDialog.h>

#include "ui_newoutputdialog.h"


namespace Ui {
class NewOutputDialog;
}

class NewOutputDialog : public QDialog
{
    Q_OBJECT

public:
    explicit NewOutputDialog(QWidget *parent = 0, QString outputId = "");
    ~NewOutputDialog();
private slots:
    void on_rbtn_hdd_clicked();

    void on_rbtn_live_clicked();

    void on_confirm_accepted();

    void on_add_input_clicked();

    void on_output_name_textChanged(const QString &arg1);

    void on_rbtn_youtube_clicked();

    void on_use_config_textChanged(const QString &arg1);

    void editStream(QString id);

    void deleteStream(QString id);

    void onSettingChanged(StreamInfo* streamInfo);
public slots:
    void onInputAdded(QList<StreamInfo*> streams);
signals:
    void saved();
private:
    Ui::NewOutputDialog *ui;
    NewInputDialog *inputDialog;
    QList<StreamInfo*> newStreams;
    QList<QString> deleteStreamIds;
    AVInfo* currentOutput;
    OutputService *outputService = OutputService::instance();
    InputOutputMapperService *mapperService = InputOutputMapperService::instance();

    void showInfo();
};

#endif // NEWOUTPUTDIALOG_H
