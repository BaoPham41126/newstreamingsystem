#ifndef VIDEOSURFACE_H
#define VIDEOSURFACE_H
extern "C" {
#include "libavformat/avformat.h"
#include <libswscale/swscale.h>
}
#include <QLabel>
#include <QMutex>
#include <QWaitCondition>
#include <QObject>

class VideoSurface : public QObject{
    Q_OBJECT

public:
    struct SwsContext* img_convert_ctx = NULL;
    int width;
    int height;
    int sourceHeight;
    QMutex mutex;
    bool stoped = true;

    VideoSurface(QLabel* widget){
        this->widget = widget;
        this->width = widget->maximumSize().width();
        this->height = widget->maximumSize().height();
        clearFrame();
    }

    void setSourceCodec(AVCodecContext* dec_ctx){
        mutex.lock();
        this->stoped = false;
        mutex.unlock();
        if (img_convert_ctx){
            sws_freeContext(img_convert_ctx);
        }
        this->img_convert_ctx = sws_getContext(
                    dec_ctx->width, dec_ctx->height, dec_ctx->pix_fmt,
                    this->width, this->height, AV_PIX_FMT_RGB24,
                    SWS_FAST_BILINEAR, NULL, NULL, NULL);
        this->sourceHeight = dec_ctx->height;
    }

    void clearFrame(){
        mutex.lock();
        this->stoped = true;
        mutex.unlock();
        QPixmap* image = new QPixmap(this->width, this->height);
        image->fill(QColor(0, 0, 0));
        widget->setPixmap(*image);
        widget->showMaximized();
    }

public slots:
    void showFrame(AVFrame* frame){
        mutex.lock();
        if (this->stoped){
            mutex.unlock();
        }
        mutex.unlock();
        QImage image(frame->data[0], width, height, frame->linesize[0], QImage::Format_RGB888);
        widget->setPixmap(QPixmap::fromImage(image));
        widget->show();
        av_free(frame);
    }
private:
    QLabel* widget;
};

#endif // VIDEOSURFACE_H
