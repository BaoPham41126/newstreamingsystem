#ifndef COURSE_H
#define COURSE_H

#include <QWidget>
#include "ui_course.h"

#include <Header/Course/CourseService.h>

namespace Ui {
class Course;
}

class Course : public QWidget
{
    Q_OBJECT

public:
    explicit Course(QWidget *parent = 0);
    ~Course();
    void loadData();

private slots:
    void on_pbtn_save_clicked();

    void on_cource_list_currentIndexChanged(int index);

    void on_insert_course_clicked();

    void on_insert_class_clicked();

    void on_pbtn_cancel_clicked();

    void on_remove_course_clicked();

    void on_remove_class_clicked();

private:
    Ui::Course *ui;
    QWidget* parentWidget;
    QList<Subject*> subjects;
    QList<Class*> classes;
    CourseService *courceService = CourseService::instance();
};

#endif // COURSE_H
