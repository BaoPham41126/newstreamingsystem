#ifndef AUDIOSETTINGDIALOG_H
#define AUDIOSETTINGDIALOG_H

#include <QDialog>
#include <Header/InputOutput/AudioStreamInfo.h>

namespace Ui {
class AudioSettingDialog;
}

class AudioSettingDialog : public QDialog
{
    Q_OBJECT

public:
    explicit AudioSettingDialog(AudioStreamInfo* tmpAudioStreamInfo, QWidget *parent = 0);
    ~AudioSettingDialog();

private:
    Ui::AudioSettingDialog *ui;
    AudioStreamInfo* tmpAudioStreamInfo;

    void showInfo();
signals:
    void settingChanged(StreamInfo* tmpAudioStreamInfo);
private slots:
    void on_buttonBox_accepted();
};

#endif // AUDIOSETTINGDIALOG_H
