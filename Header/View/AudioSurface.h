#ifndef AUDIOSURFACE_H
#define AUDIOSURFACE_H
extern "C" {
#include "libavformat/avformat.h"
}
#include <QObject>
#include <QMutex>
#include <QProgressBar>

class AudioSurface : public QObject{
    Q_OBJECT

public:
    QMutex mutex;
    bool stoped = true;

    AudioSurface(QProgressBar* processBar){
        this->processBar = processBar;
        this->processBar->setTextVisible(false);
        clearFrame();
    }

    void clearFrame(){
        mutex.lock();
        this->stoped = true;
        mutex.unlock();
        this->processBar->setValue(0);
    }

public slots:
    void showFrame(AVFrame* frame){
        mutex.lock();
        if (this->stoped){
            mutex.unlock();
        }
        mutex.unlock();
        float  window;
        float sum = 0;
        //read audio sample
        for (int i = 0; i < frame->nb_samples * 2; ++i){
            window = (uint8_t)frame->data[0][i] / 32768.0f;
            sum += window * window;
        }
        //calculate audio level meter
        float rms = sqrt(sum / frame->nb_samples);
        float decibel = (-90 - 20 * log(rms))/23 ;
        decibel = decibel > 1.0 ? 1.0: (decibel < 0.0 ? 0.0: decibel);
        int data = decibel * 100;
        this->processBar->setValue(100 - data);
    }
private:
    QProgressBar* processBar;
};

#endif // AUDIOSURFACE_H
