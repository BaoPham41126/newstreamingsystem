#ifndef RECORDING_H
#define RECORDING_H

#include <QWidget>
#include <QListWidgetItem>
#include <QTableWidgetItem>

#include <Header/Record/RecordService.h>
#include <Header/View/NewOutputDialog.h>
#include "ui_recording.h"

namespace Ui {
class Recording;
}

class Recording : public QWidget
{
    Q_OBJECT

public:
    explicit Recording(QWidget *parent = 0);
    ~Recording();
    AudioSurface* addNewAudioSurface(QString name);
    void clearSurface();

private slots:
    void on_qbtn_back_clicked();

    void on_start_btn_clicked();

    void on_pause_btn_clicked();

    void on_stop_btn_clicked();

    void on_preview_btn_clicked();

    void on_btn_add_clicked();

    void showOutputInfo();

    void editOutput(QString id);

    void deleteOutput(QString id);
private:
    Ui::Recording *ui;
    QWidget* parentWidget;
    RecordService* recordService = RecordService::instance();
    OutputService* outputService = OutputService::instance();

    void setRecording(bool recording);
};

#endif // RECORDING_H
