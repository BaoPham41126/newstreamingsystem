#ifndef VIDEOSETTINGDIALOG_H
#define VIDEOSETTINGDIALOG_H

#include <QDialog>
#include <Header/InputOutput/VideoStreamInfo.h>

namespace Ui {
class VideoSettingDialog;
}

class VideoSettingDialog : public QDialog
{
    Q_OBJECT

public:
    explicit VideoSettingDialog(VideoStreamInfo* tmpVideoStreamInfo, QWidget *parent = 0);
    ~VideoSettingDialog();

private:
    Ui::VideoSettingDialog *ui;
    VideoStreamInfo* tmpVideoStreamInfo;

    void showInfo();
signals:
    void settingChanged(StreamInfo* streamInfo);
private slots:
    void on_buttonBox_accepted();
};

#endif // VIDEOSETTINGDIALOG_H
