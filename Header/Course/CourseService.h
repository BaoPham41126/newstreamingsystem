/***********************************************************************
 * Module:  CourseService.h
 * Author:  BaoPham
 * Modified: Wednesday, November 22, 2017 10:19:35 PM
 * Purpose: Declaration of the class CourseService
 ***********************************************************************/

#if !defined(__Course_CourseService_h)
#define __Course_CourseService_h

#include <QObject>
#include <QList>
#include <QUuid>
#include <QFile>

#include <Header/Base/Singleton.h>
#include <Header/Course/Lecture.h>
#include <Header/Course/Class.h>
#include <Header/Course/Subject.h>

class CourseService : public QObject
{
    Q_OBJECT

public:
   Lecture* saveLecture(Lecture* lecture);
   Class* saveClass(Class* _class);
   Subject* saveSubject(Subject* subject);
   void deleteLecture(QString id);
   void deleteClass(QString id);
   void deleteSubject(QString id);
   Class* getCurrentClass();
   void setCurrentClass(Class* lecture);
   QList<Subject*> getAllSubject();
   QList<Class*> getClassOfSubject(QString subjectId);
   QList<Lecture*> getLectureOfClass(QString classId);
   Lecture* getLecture(QString id);
   Class* getClass(QString id);
   Subject* getSubject(QString id);

   ~CourseService(){
       this->saveData();
       subjects.clear();
       _classes.clear();
       lectures.clear();
   }
   static CourseService* instance(){
       return Singleton<CourseService>::instance(CourseService::createInstance);
   }
protected:
   int _getLectureIndex(QString id);
   int _getClassIndex(QString id);
   int _getSubjectIndex(QString id);
private:
   CourseService(QObject* parent = 0) : QObject(parent){
       this->loadData();
   }
   static CourseService* createInstance(){ return new CourseService();}
   QList<Subject*> subjects;
   QList<Class*> _classes;
   QList<Lecture*> lectures;
   Class* currentClass = NULL;

   QString dataFileString = "data/course.txt";

   void loadData();
   void saveData();
};

#endif
