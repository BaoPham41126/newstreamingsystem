/***********************************************************************
 * Module:  Subject.h
 * Author:  BaoPham
 * Modified: Wednesday, November 22, 2017 10:18:21 PM
 * Purpose: Declaration of the class Subject
 ***********************************************************************/

#if !defined(__Course_Subject_h)
#define __Course_Subject_h
#include <QString>
#include <QDataStream>
#include <Header/Course/Class.h>

class Subject
{
public:
   QString id;
   QString name;

   friend QDataStream &operator<<(QDataStream &out, const Subject &info){
       info.write(out);
       return out;
   }

   virtual void write(QDataStream &out) const {
       out << this->id << this->name;
   }

   friend QDataStream &operator>>(QDataStream &in, Subject &info){
       info.read(in);
       return in;
   }

   virtual void read(QDataStream &in){
       in >> this->id >> this->name;
   }
};

#endif
