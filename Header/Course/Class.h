/***********************************************************************
 * Module:  Class.h
 * Author:  BaoPham
 * Modified: Wednesday, November 22, 2017 10:18:35 PM
 * Purpose: Declaration of the class Class
 ***********************************************************************/

#if !defined(__Course_Class_h)
#define __Course_Class_h

#include <Header/Course/Lecture.h>
#include <QDataStream>
#include <QString>

class Class {
public:
   QString id;
   QString subjectId;
   QString name;

   friend QDataStream &operator<<(QDataStream &out, const Class &info){
       info.write(out);
       return out;
   }

   virtual void write(QDataStream &out) const {
       out << this->id << this->subjectId << this->name;
   }

   friend QDataStream &operator>>(QDataStream &in, Class &info){
       info.read(in);
       return in;
   }

   virtual void read(QDataStream &in){
       in >> this->id >> this->subjectId >> this->name;
   }
};

#endif
