/***********************************************************************
 * Module:  Lecture.h
 * Author:  BaoPham
 * Modified: Wednesday, November 22, 2017 10:18:50 PM
 * Purpose: Declaration of the class Lecture
 ***********************************************************************/

#if !defined(__Course_Lecture_h)
#define __Course_Lecture_h
#include <QString>
#include <QDataStream>

class Lecture
{
public:
   QString id;
   QString classId;
   QString name;

   friend QDataStream &operator<<(QDataStream &out, const Lecture &info){
       info.write(out);
       return out;
   }

   virtual void write(QDataStream &out) const {
       out << this->id << this->classId << this->name;
   }

   friend QDataStream &operator>>(QDataStream &in, Lecture &info){
       info.read(in);
       return in;
   }

   virtual void read(QDataStream &in){
       in >> this->id >> this->classId >> this->name;
   }
};

#endif
