/***********************************************************************
 * Module:  CodecService.h
 * Author:  BaoPham
 * Modified: Wednesday, November 22, 2017 10:10:50 PM
 * Purpose: Declaration of the class CodecService
 ***********************************************************************/

#if !defined(__Codec_CodecService_h)
#define __Codec_CodecService_h

extern "C" {
#include "libavformat/avformat.h"
#include "libavdevice/avdevice.h"
}
#include <QObject>
#include <Header/Base/Singleton.h>
#include "Header/InputOutput/VideoStreamInfo.h"
#include "Header/InputOutput/AudioStreamInfo.h"
#include "Header/Stream/AVStream.h"

class CodecService : public QObject
{
    Q_OBJECT

public:
   AVCodecContext* createOutputCodecContext(Stream *stream, StreamInfo* info);
   AVCodecContext* createInputCodecContext(Stream *stream, StreamInfo* info);

   ~CodecService(){ }
   static CodecService* instance(){
       return Singleton<CodecService>::instance(CodecService::createInstance);
   }
protected:
private:
   CodecService(QObject* parent = 0) : QObject(parent){}
   static CodecService* createInstance(){ return new CodecService();}

   AVCodecContext* _createVideoCodecContext(Stream *stream, VideoStreamInfo* info);
   AVCodecContext* _createAudioCodecContext(Stream *stream, AudioStreamInfo* info);
};

#endif
