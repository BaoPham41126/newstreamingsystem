/***********************************************************************
 * Module:  OutputStream.h
 * Author:  BaoPham
 * Modified: Wednesday, November 22, 2017 9:14:49 PM
 * Purpose: Declaration of the class OutputStream
 ***********************************************************************/

#if !defined(__Stream_OutputStream_h)
#define __Stream_OutputStream_h

#include <Header/Stream/AVStream.h>

class OutputStream : public Stream
{
    Q_OBJECT

public:
   int writePacket(AVPacket* packet);
};

#endif
