/***********************************************************************
 * Module:  StreamService.h
 * Author:  BaoPham
 * Modified: Wednesday, November 22, 2017 10:06:03 PM
 * Purpose: Declaration of the class StreamService
 ***********************************************************************/

#if !defined(__Stream_StreamService_h)
#define __Stream_StreamService_h

#include <QObject>
#include <QMap>
#include <QMutex>

#include <Header/Base/Singleton.h>
#include <Header/Stream/AVStream.h>
#include <Header/Stream/InputStream.h>
#include <Header/Stream/OutputStream.h>
#include <Header/InputOutput/AVInfo.h>
#include <Header/InputOutput/AudioStreamInfo.h>
#include <Header/InputOutput/VideoStreamInfo.h>
#include <Header/Codec/CodecService.h>
#include <Header/Course/CourseService.h>

class StreamService : public QObject
{
    Q_OBJECT

public:
   QList<Stream*> createStreams(AVInfo* info);
   ~StreamService(){}
   void clearData(){
       totalIn = 0;
       totalOut = 0;
       currentSession = -1;
   }

   int totalIn = 0;
   int totalOut = 0;

   static StreamService* instance(){
       return Singleton<StreamService>::instance(StreamService::createInstance);
   }

protected:
   int currentSession = -1;
private:
   StreamService(QObject* parent = 0) : QObject(parent){}
   static StreamService* createInstance(){ return new StreamService();}

   CodecService* codecService = CodecService::instance();
   CourseService* courseService = CourseService::instance();

   QList<Stream*> createInputStreams(AVInfo* info);
   QList<Stream*> createOutputStreams(AVInfo* info);

   Stream* createOutputStream(AVFormatContext* format,
                              QMutex* streamMutex,
                              StreamInfo* info);
};

#endif
