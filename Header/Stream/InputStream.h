/***********************************************************************
 * Module:  InputStream.h
 * Author:  BaoPham
 * Modified: Wednesday, November 22, 2017 9:14:45 PM
 * Purpose: Declaration of the class InputStream
 ***********************************************************************/

#if !defined(__Stream_InputStream_h)
#define __Stream_InputStream_h

#include <Header/Stream/AVStream.h>

class InputStream : public Stream
{
    Q_OBJECT

public:
   int readPacket(AVPacket* packet);
};

#endif
