/***********************************************************************
 * Module:  AVStream.h
 * Author:  BaoPham
 * Modified: Thursday, November 23, 2017 9:28:16 PM
 * Purpose: Declaration of the class AVStream
 ***********************************************************************/

#if !defined(__Stream_AVStream_h)
#define __Stream_AVStream_h
extern "C" {
#include "libavformat/avformat.h"
}
#include <QObject>
#include <QMutex>
#include <QWaitCondition>

class Stream : public QObject
{
    Q_OBJECT

public:
    QString infoId;
    AVFormatContext* format;
    AVStream* avStream;

    //Output only
    QWaitCondition* waitMainStream;
    QMutex* mutex;
    int queueSize;
    bool mainStream = false;

protected:
private:
};

#endif
