/***********************************************************************
 * Module:  RecordService.h
 * Author:  BaoPham
 * Modified: Friday, November 24, 2017 1:14:28 AM
 * Purpose: Declaration of the class RecordService
 ***********************************************************************/

#if !defined(__Record_RecordService_h)
#define __Record_RecordService_h

#include <QObject>
#include <QVideoSurfaceFormat>

#include <Header/Base/Singleton.h>
#include <Header/InputOutput/InputService.h>
#include <Header/InputOutput/OutputService.h>
#include <Header/Thread/IOThreadService.h>
#include <Header/Stream/StreamService.h>
#include <Header/Buffer/FrameBufferService.h>
#include <Header/View/VideoSurface.h>
#include <Header/View/AudioSurface.h>

class Recording;

class RecordService : public QObject
{
    Q_OBJECT

public:
   int start();
   int stop();
   int review();
   int pause();

   ~RecordService(){ }
   static RecordService* instance(){
       return Singleton<RecordService>::instance(RecordService::createInstance);
   }
   void setRecordView(Recording* recordingView){
       this->recordingView = recordingView;
   }

   VideoSurface* leftVideo;
   VideoSurface* rightVideo;
   Recording* recordingView;
private slots:
   void getLeftFrame(AVFrame* frame) {
       leftVideo->showFrame(frame);
   }
   void getRightFrame(AVFrame* frame) {
       rightVideo->showFrame(frame);
   }
protected:
private:
   RecordService(QObject* parent = 0) : QObject(parent){ }
   static RecordService* createInstance(){ return new RecordService();}

   InputService* inputService = InputService::instance();
   OutputService* outputService = OutputService::instance();
   IOThreadService* threadService = IOThreadService::instance();
   StreamService* streamService = StreamService::instance();
   FrameBufferService* bufferService = FrameBufferService::instance();
   QList<IOThread*> inputWorkers;
   QList<IOThread*> outputWorkers;

   void initInputWorker();
   void initOutputWorker();
};

#endif
