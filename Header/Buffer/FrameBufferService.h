/***********************************************************************
 * Module:  FrameBufferService.h
 * Author:  BaoPham
 * Modified: Wednesday, November 22, 2017 10:10:50 PM
 * Purpose: Declaration of the class FrameBufferService
 ***********************************************************************/

#if !defined(__FrameBuffer_FrameBufferService_h)
#define __FrameBuffer_FrameBufferService_h

#include <QObject>
#include <Header/Base/Singleton.h>
#include <Header/Buffer/FrameBuffer.h>
#include <Header/InputOutput/StreamInfo.h>

#include <QList>

class FrameBufferService : public QObject
{
    Q_OBJECT

public:
   FrameBuffer* createBuffer(QString outputId, int queueSize, AVCodecContext* codec);
   FrameBuffer* getBufferByOutputId(QString outputId);
   int deleteBuffer(QString id);
   void deleteAllBuffer();

   ~FrameBufferService(){ deleteAllBuffer(); }
   static FrameBufferService* instance(){
       return Singleton<FrameBufferService>::instance(FrameBufferService::createInstance);
   }
protected:
private:
   FrameBufferService(QObject* parent = 0) : QObject(parent){}
   static FrameBufferService* createInstance(){ return new FrameBufferService();}
   QList<FrameBuffer*> buffers;
};

#endif
