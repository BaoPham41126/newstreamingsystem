/***********************************************************************
 * Module:  FrameBuffer.h
 * Author:  BaoPham
 * Modified: Wednesday, November 22, 2017 9:54:56 PM
 * Purpose: Declaration of the class FrameBuffer
 ***********************************************************************/

#if !defined(__FrameBuffer_FrameBuffer_h)
#define __FrameBuffer_FrameBuffer_h
extern "C" {
#include "libavformat/avformat.h"
#include "libavutil/fifo.h"
}
#include <QQueue>
#include <QWaitCondition>
#include <QMutex>
#include <QUuid>
#include "Header/InputOutput/StreamInfo.h"
#include "Header/InputOutput/AudioStreamInfo.h"

class FrameBuffer {
public:
   AVFrame* getFrame();
   int putFrame(AVFrame* frame);
   FrameBuffer(QString outputId, int queueSize, AVCodecContext* codec);
   ~FrameBuffer();

   QString id;
   QString outputId;
   QQueue<AVFrame*> frames;
   int maxSize;

   QWaitCondition bufferNotEmpty;
   QMutex mutex;
   AVCodecContext* codec;

   //For Audio
   AVFifoBuffer* fifo;
   uint8_t* o_samples;
   int64_t sampleSize = -1;
};

#endif
