#ifndef VIDEOSETTINGDIALOG_H
#define VIDEOSETTINGDIALOG_H

#include <QDialog>

namespace Ui {
class VideoSettingDialog;
}

class VideoSettingDialog : public QDialog
{
    Q_OBJECT

public:
    explicit VideoSettingDialog(QWidget *parent = 0);
    ~VideoSettingDialog();

private:
    Ui::VideoSettingDialog *ui;
};

#endif // VIDEOSETTINGDIALOG_H
