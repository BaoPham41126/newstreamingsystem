#include "Header/Youtube/YoutubeService.h"
#include <QUrl>
#include <QUrlQuery>
#include <QtNetwork/QNetworkAccessManager>
#include <QtNetwork/QNetworkRequest>

#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>

#include <iostream>

void YoutubeService::setCredential(Credential *credential){
    this->credential = credential;
}

void YoutubeService::createNewBroadcast(QString title, QDateTime startTime, QString privacyStatus){
    static QString URL = "https://www.googleapis.com/youtube/v3/liveBroadcasts";
    QNetworkAccessManager *manager = new QNetworkAccessManager(this);
    QUrl url(URL);

    //prepare url
    QUrlQuery urlQuery;
    urlQuery.addQueryItem("part", "snippet,contentDetails,status");
    url.setQuery(urlQuery);

    //prepare request and header
    QNetworkRequest request(url);
    request.setHeader(QNetworkRequest::ContentTypeHeader,"application/json");
    QString authorizationData = "Bearer " + this->credential->accessToken;
    request.setRawHeader("Authorization", authorizationData.toLocal8Bit());

    //prepare data
    QJsonObject broadcast;
    broadcast["kind"] = QString("youtube#liveBroadcast");

    QJsonObject snippet;
    snippet["title"] = title;
    snippet["scheduledStartTime"] = startTime.toString("yyyy-MM-dd'T'HH:mm:ss+07:00");
    std::cout << startTime.toString("yyyy-MM-dd'T'HH:mm:ss+07:00").toStdString() << std::endl;
    broadcast["snippet"] = snippet;

    QJsonObject status;
    std::cout << privacyStatus.toStdString() << std::endl;
    status["privacyStatus"] = privacyStatus;
    broadcast["status"] = status;

    QJsonObject contentDetails;
    QJsonObject monitorStream;
    monitorStream["enableMonitorStream"] = false;
    contentDetails["monitorStream"] = monitorStream;
    broadcast["contentDetails"] = contentDetails;

    QObject::connect(manager, &QNetworkAccessManager::finished, this, &YoutubeService::onBroadcastCreated);
    manager->post(request, QJsonDocument(broadcast).toJson());
}

Broadcast* _createBroadcastFromReply(QNetworkReply *reply){
    QString data = (QString) reply->readAll();
    QJsonDocument jsonResponse = QJsonDocument::fromJson(data.toUtf8());
    QJsonObject json = jsonResponse.object();
    Broadcast *broadcast = new Broadcast();
    broadcast->id = json["id"].toString();
    QJsonObject snippet = json["snippet"].toObject();
    broadcast->title = snippet["title"].toString();
    QJsonObject status = json["status"].toObject();
    broadcast->privacyStatus = status["privacyStatus"].toString();
    broadcast->lifeCycleStatus = status["lifeCycleStatus"].toString();
    QJsonObject contentDetails = json["contentDetails"].toObject();
    broadcast->boundStreamId = contentDetails["boundStreamId"].toString();
    return broadcast;
}

void YoutubeService::onBroadcastCreated(QNetworkReply *reply){
    if (reply->error() != QNetworkReply::NoError){
        std::cout << reply->errorString().toStdString() << std::endl;
        return;
    }
    Broadcast *broadcast = _createBroadcastFromReply(reply);
    Q_EMIT broadcastCreated(broadcast);
}

void YoutubeService::createNewStream(QString title, QString cdnFormat){
    static QString URL = "https://www.googleapis.com/youtube/v3/liveStreams";
    QNetworkAccessManager *manager = new QNetworkAccessManager(this);
    QUrl url(URL);

    //prepare url
    QUrlQuery urlQuery;
    urlQuery.addQueryItem("part", "snippet,status,cdn");
    url.setQuery(urlQuery);

    //prepare request and header
    QNetworkRequest request(url);
    request.setHeader(QNetworkRequest::ContentTypeHeader,"application/json");
    QString authorizationData = "Bearer " + this->credential->accessToken;
    request.setRawHeader("Authorization", authorizationData.toLocal8Bit());

    //prepare data
    QJsonObject stream;
    stream["kind"] = QString("youtube#liveStream");

    QJsonObject snippet;
    snippet["title"] = title;
    stream["snippet"] = snippet;

    QJsonObject cdn;
    cdn["format"] = cdnFormat;
    cdn["ingestionType"] = "rtmp";
    stream["cdn"] = cdn;

    QObject::connect(manager, &QNetworkAccessManager::finished, this, &YoutubeService::onStreamCreated);
    manager->post(request, QJsonDocument(stream).toJson());
}

YoutubeStream* _createStreamFromReply(QNetworkReply *reply, bool fromList){
    QString data = (QString) reply->readAll();
    QJsonDocument jsonResponse = QJsonDocument::fromJson(data.toUtf8());
    QJsonObject json;
    if (!fromList)
        json = jsonResponse.object();
    else
        json = jsonResponse.object()["items"].toArray().at(0).toObject();
    YoutubeStream *stream = new YoutubeStream();
    stream->id = json["id"].toString();
    QJsonObject ingestionInfo = (json["cdn"].toObject())["ingestionInfo"].toObject();
    stream->address = ingestionInfo["ingestionAddress"].toString() + "/" + ingestionInfo["streamName"].toString();
    QJsonObject snippet = json["snippet"].toObject();
    stream->title = snippet["title"].toString();
    QJsonObject status = json["status"].toObject();
    stream->streamStatus = status["streamStatus"].toString();
    QJsonObject healthStatus = status["healthStatus"].toObject();
    stream->healthStatus = healthStatus["status"].toString();
    return stream;
}

void YoutubeService::onStreamCreated(QNetworkReply *reply){
    if (reply->error() != QNetworkReply::NoError){
        std::cout << reply->errorString().toStdString() << std::endl;
        return;
    }
    YoutubeStream *stream = _createStreamFromReply(reply, false);
    Q_EMIT streamCreated(stream);
}

void YoutubeService::getStream(QString streamId){
    static QString streamURL = "https://www.googleapis.com/youtube/v3/liveStreams";
    QNetworkAccessManager *manager = new QNetworkAccessManager(this);

    QUrl url(streamURL);
    QUrlQuery query;
    query.addQueryItem("id", streamId);
    query.addQueryItem("part", "id,snippet,cdn,status");
    url.setQuery(query);

    QNetworkRequest request(url);
    request.setHeader(QNetworkRequest::ContentTypeHeader,"application/json");
    QString authorizationData = "Bearer " + this->credential->accessToken;
    request.setRawHeader("Authorization", authorizationData.toLocal8Bit());

    QObject::connect(manager, &QNetworkAccessManager::finished, this, &YoutubeService::onStreamReturn);
    manager->get(request);
}

void YoutubeService::onStreamReturn(QNetworkReply *reply){
    if (reply->error() != QNetworkReply::NoError){
        std::cout << reply->errorString().toStdString() << std::endl;
        return;
    }
    YoutubeStream *stream = _createStreamFromReply(reply, true);
    Q_EMIT streamReturn(stream);
}

void YoutubeService::bindStreamToBroadcast(QString streamId, QString broadcastId){
    static QString URL = "https://www.googleapis.com/youtube/v3/liveBroadcasts/bind";
    QNetworkAccessManager *manager = new QNetworkAccessManager(this);
    QUrl url(URL);

    //prepare url
    QUrlQuery urlQuery;
    urlQuery.addQueryItem("part", "snippet,contentDetails,status");
    urlQuery.addQueryItem("id", broadcastId);
    urlQuery.addQueryItem("streamId", streamId);
    url.setQuery(urlQuery);

    //prepare request and header
    QNetworkRequest request(url);
    request.setHeader(QNetworkRequest::ContentTypeHeader,"application/json");
    QString authorizationData = "Bearer " + this->credential->accessToken;
    request.setRawHeader("Authorization", authorizationData.toLocal8Bit());

    //prepare data
    QJsonObject bindData;

    QObject::connect(manager, &QNetworkAccessManager::finished, this, &YoutubeService::onBindCompleted);
    manager->post(request, QJsonDocument(bindData).toJson());
}

void YoutubeService::onBindCompleted(QNetworkReply *reply){
    if (reply->error() != QNetworkReply::NoError){
        std::cout << reply->errorString().toStdString() << std::endl;
        return;
    }
    Broadcast *broadcast = _createBroadcastFromReply(reply);
    Q_EMIT bindCompleted(broadcast);
}

void YoutubeService::changeBroadcastStatus(QString broadcastId, QString status){
    static QString URL = "https://www.googleapis.com/youtube/v3/liveBroadcasts/transition";
    QNetworkAccessManager *manager = new QNetworkAccessManager(this);
    QUrl url(URL);

    //prepare url
    QUrlQuery urlQuery;
    urlQuery.addQueryItem("part", "snippet,contentDetails,status");
    urlQuery.addQueryItem("id", broadcastId);
    urlQuery.addQueryItem("broadcastStatus", status);
    std::cout << "Change broadcast " <<broadcastId.toStdString() << " to " << status.toStdString();
    url.setQuery(urlQuery);

    //prepare request and header
    QNetworkRequest request(url);
    request.setHeader(QNetworkRequest::ContentTypeHeader,"application/json");
    QString authorizationData = "Bearer " + this->credential->accessToken;
    request.setRawHeader("Authorization", authorizationData.toLocal8Bit());

    //prepare data
    QJsonObject transitionData;

    QObject::connect(manager, &QNetworkAccessManager::finished, this, &YoutubeService::onTransitionCompleted);
    manager->post(request, QJsonDocument(transitionData).toJson());
}

void YoutubeService::onTransitionCompleted(QNetworkReply *reply){
    if (reply->error() != QNetworkReply::NoError){
        std::cout << reply->errorString().toStdString() << std::endl;
        return;
    }
    Broadcast *broadcast = _createBroadcastFromReply(reply);
    Q_EMIT transitionCompleted(broadcast);
}

void YoutubeService::getNextBroadcastPage(QString pageToken){
    static QString streamURL = "https://www.googleapis.com/youtube/v3/liveBroadcasts";
    QNetworkAccessManager *manager = new QNetworkAccessManager(this);

    QUrl url(streamURL);
    QUrlQuery query;
    query.addQueryItem("part", "snippet,contentDetails,status");
    query.addQueryItem("maxResults", "5");
    query.addQueryItem("pageToken", pageToken);
    query.addQueryItem("mine", "true");
    url.setQuery(query);

    QNetworkRequest request(url);
    request.setHeader(QNetworkRequest::ContentTypeHeader,"application/json");
    QString authorizationData = "Bearer " + this->credential->accessToken;
    request.setRawHeader("Authorization", authorizationData.toLocal8Bit());

    QObject::connect(manager, &QNetworkAccessManager::finished, this, &YoutubeService::onAllBroadcastReturn);
    manager->get(request);
}

void YoutubeService::getAllBroadcast(){
    static QString streamURL = "https://www.googleapis.com/youtube/v3/liveBroadcasts";
    QNetworkAccessManager *manager = new QNetworkAccessManager(this);

    QUrl url(streamURL);
    QUrlQuery query;
    query.addQueryItem("part", "snippet,contentDetails,status");
    query.addQueryItem("maxResults", "5");
    query.addQueryItem("mine", "true");
    url.setQuery(query);

    QNetworkRequest request(url);
    request.setHeader(QNetworkRequest::ContentTypeHeader,"application/json");
    QString authorizationData = "Bearer " + this->credential->accessToken;
    request.setRawHeader("Authorization", authorizationData.toLocal8Bit());

    QObject::connect(manager, &QNetworkAccessManager::finished, this, &YoutubeService::onAllBroadcastReturn);
    manager->get(request);
}

void YoutubeService::_createMultiBroadcastFromReply(QNetworkReply *reply){
    QString data = (QString) reply->readAll();
    QJsonDocument jsonResponse = QJsonDocument::fromJson(data.toUtf8());
    QJsonObject json = jsonResponse.object();
    QJsonArray broadcast_array = json["items"].toArray();
    if (broadcast_array.size() == 0) Q_EMIT allBroadcastReturn(NULL, 0);
    std::cout << broadcast_array.size() << std::endl;
    Broadcast** output = new Broadcast*[broadcast_array.size()];
    for (int i = 0; i < broadcast_array.size(); i++){
        QJsonObject json = broadcast_array.at(i).toObject();
        Broadcast *broadcast = new Broadcast();
        broadcast->id = json["id"].toString();
        QJsonObject snippet = json["snippet"].toObject();
        broadcast->title = snippet["title"].toString();
        QJsonObject status = json["status"].toObject();
        broadcast->privacyStatus = status["privacyStatus"].toString();
        broadcast->lifeCycleStatus = status["lifeCycleStatus"].toString();
        QJsonObject contentDetails = json["contentDetails"].toObject();
        broadcast->boundStreamId = contentDetails["boundStreamId"].toString();
        output[i] = broadcast;
    }
    LoadBroadcastInfo *loader = new LoadBroadcastInfo(output, broadcast_array.size(), this->credential);
    QObject::connect(loader, &LoadBroadcastInfo::allBroadcastLoadComplete, this, &YoutubeService::loadBroadcastsComplete);
    loader->load();
    if (json.contains("nextPageToken")){
        QString nextPageToken = json["nextPageToken"].toString();
        std::cout << nextPageToken.toStdString() << std::endl;
        this->getNextBroadcastPage(nextPageToken);
    }
}

void YoutubeService::loadBroadcastsComplete(Broadcast **broadcast, int size){
    Q_EMIT allBroadcastReturn(broadcast, size);
}

void YoutubeService::onAllBroadcastReturn(QNetworkReply *reply){
    if (reply->error() != QNetworkReply::NoError){
        std::cout << reply->errorString().toStdString() << std::endl;
        return;
    }
    this->_createMultiBroadcastFromReply(reply);
}

void YoutubeService::deleteBroadcast(QString broadcastId){
    static QString streamURL = "https://www.googleapis.com/youtube/v3/liveBroadcasts";
    QNetworkAccessManager *manager = new QNetworkAccessManager(this);

    QUrl url(streamURL);
    QUrlQuery query;
    query.addQueryItem("id", broadcastId);
    url.setQuery(query);

    QNetworkRequest request(url);
    request.setHeader(QNetworkRequest::ContentTypeHeader,"application/json");
    QString authorizationData = "Bearer " + this->credential->accessToken;
    request.setRawHeader("Authorization", authorizationData.toLocal8Bit());
    std::cout << "Delete " << broadcastId.toStdString() << std::endl;
    QObject::connect(manager, &QNetworkAccessManager::finished, this, &YoutubeService::onBroadcastDeleted);
    manager->deleteResource(request);
}

void YoutubeService::onBroadcastDeleted(QNetworkReply *reply){
    if (reply->error() != QNetworkReply::NoError){
        std::cout << reply->errorString().toStdString() << std::endl;
        return;
    }
    else{
        Broadcast *broadcast = _createBroadcastFromReply(reply);
        Q_EMIT broadcastDeleted(broadcast->id);
    }
}

void LoadBroadcastInfo::load(){
    for(int i = 0; i < total; i++){
        YoutubeService* youtube = YoutubeService::instance();
        QObject::connect(youtube, &YoutubeService::streamReturn, this, &LoadBroadcastInfo::onStreamReturn);
        youtube->getStream(broadcasts[i]->boundStreamId);
    }
}

void LoadBroadcastInfo::onStreamReturn(YoutubeStream *stream){
    for(int i = 0; i < total; i++){
        if (broadcasts[i]->boundStreamId.compare(stream->id) == 0){
            broadcasts[i]->boundStreamAddress = stream->address;
        }
    }
    this->less -= 1;
    if (this->less == 0){
        Q_EMIT allBroadcastLoadComplete(this->broadcasts, this->total);
    }
}
