/***********************************************************************
 * Module:  FrameBuffer.cpp
 * Author:  BaoPham
 * Modified: Wednesday, November 22, 2017 9:54:56 PM
 * Purpose: Implementation of the class FrameBuffer
 ***********************************************************************/

#include "Header/Buffer/FrameBuffer.h"
#include "iostream"

int64_t cal_sample_size(AVCodecContext* codec) {
    int osize = av_get_bytes_per_sample(codec->sample_fmt);
    int64_t sample_size = codec->frame_size * osize * codec->channels;
    return sample_size;
}

FrameBuffer::FrameBuffer(QString outputId, int queueSize, AVCodecContext* codec){
    //set defaule value
    QUuid uuid = QUuid::createUuid();
    this->id = uuid.toString();
    this->outputId = outputId;
    this->maxSize = queueSize;
    this->codec = codec;
    if (codec->codec_type == AVMEDIA_TYPE_AUDIO){
        this->sampleSize = cal_sample_size(codec);
        if (this->sampleSize != -1){
            this->o_samples = (uint8_t*) malloc(sampleSize);
            this->fifo = av_fifo_alloc(10 * sampleSize);
        }
    }
}

FrameBuffer::~FrameBuffer(){
    mutex.lock();
    while(!this->frames.isEmpty()){
        AVFrame* frame = this->frames.dequeue();
        av_free(frame);
    }
    qInfo("Clear buffer complete");
    mutex.unlock();
}

AVFrame* FrameBuffer::getFrame(){
    mutex.lock();
    if (this->frames.empty()) {
        bufferNotEmpty.wait(&mutex);
    }
    mutex.unlock();
    if (this->frames.empty()) {
        return NULL;
    }
    return this->frames.dequeue();
}

int i = 0;

int FrameBuffer::putFrame(AVFrame* frame){
    mutex.lock();
    if (this->frames.size() > this->maxSize){
        this->frames.dequeue();
    }
    mutex.unlock();

    this->frames.enqueue(frame);

    mutex.lock();
    bufferNotEmpty.wakeAll();
    mutex.unlock();
    return 0;
}
