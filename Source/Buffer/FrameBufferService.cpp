/***********************************************************************
 * Module:  FrameBufferService.cpp
 * Author:  BaoPham
 * Modified: Wednesday, November 22, 2017 10:10:50 PM
 * Purpose: Implementation of the class FrameBufferService
 ***********************************************************************/

#include "Header/Buffer/FrameBufferService.h"

FrameBuffer* FrameBufferService::createBuffer(QString outputId, int queueSize, AVCodecContext* codec) {
    FrameBuffer* newBuffer = new FrameBuffer(outputId, queueSize, codec);
    this->buffers.append(newBuffer);
    return newBuffer;
}

FrameBuffer* FrameBufferService::getBufferByOutputId(QString outputId) {
    for (int i = 0; i< this->buffers.size(); i++){
        if (this->buffers.at(i)->outputId == outputId){
            return this->buffers.at(i);
        }
    }
    return NULL;
}

int FrameBufferService::deleteBuffer(QString id) {
    for (int i = 0; i< this->buffers.size(); i++){
        if (this->buffers.at(i)->id == id){
            this->buffers.removeAt(i);
            return 0;
        }
    }
    return -1;
}

void FrameBufferService::deleteAllBuffer(){
    qDeleteAll(this->buffers);
    this->buffers.clear();
}
