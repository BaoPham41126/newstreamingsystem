/***********************************************************************
 * Module:  RecordService.cpp
 * Author:  BaoPham
 * Modified: Friday, November 24, 2017 1:14:28 AM
 * Purpose: Implementation of the class RecordService
 ***********************************************************************/

#include "Header/Record/RecordService.h"
#include "Header/View/Recording.h"

void RecordService::initInputWorker(){
    bool left = true;
    QList<AVInfo*> inputs = inputService->getAllInput();
    for (int i = 0; i < inputs.size(); i++){
        QList<IOThread*> _inputWorkers = threadService->createIOThread(inputs.at(i));
        for (int j = 0; j < _inputWorkers.size(); j++) {
            InputThread* inputWorker = (InputThread*) _inputWorkers.at(j);
            inputWorker->init();
            if (inputWorker->stream->avStream->codec->codec_type == AVMEDIA_TYPE_VIDEO){
                if(left){
                    this->leftVideo->setSourceCodec(inputWorker->stream->avStream->codec);
                    inputWorker->reviewPlayer = leftVideo;
                    connect(inputWorker, SIGNAL(frameChange(AVFrame*)), leftVideo, SLOT(showFrame(AVFrame*)));
                    left = false;
                } else {
                    this->rightVideo->setSourceCodec(inputWorker->stream->avStream->codec);
                    inputWorker->reviewPlayer = rightVideo;
                    connect(inputWorker, SIGNAL(frameChange(AVFrame*)), rightVideo, SLOT(showFrame(AVFrame*)));
                }
            } else {
                AudioSurface* audioSurface = recordingView->addNewAudioSurface(inputs.at(i)->name);
                inputWorker->audioReviewPlayer = audioSurface;
                connect(inputWorker, SIGNAL(frameChange(AVFrame*)), audioSurface, SLOT(showFrame(AVFrame*)));
            }
            this->inputWorkers.append(inputWorker);
        }
    }
}

void RecordService::initOutputWorker(){
    QList<AVInfo*> outputs = outputService->getAllActiveOutput();
    for (int i = 0; i < outputs.size(); i++){
        QList<IOThread*> _outputWorkers = threadService->createIOThread(outputs.at(i));
        for (int j = 0; j < _outputWorkers.size(); j++) {
            _outputWorkers.at(j)->init();
            this->outputWorkers.append(_outputWorkers.at(j));
        }
    }
}

int RecordService::start(){
    this->initOutputWorker();
    this->initInputWorker();

    for (int i = 0; i < inputWorkers.size(); i++){
        inputWorkers.at(i)->thread->start();
    }

    for (int i = 0; i < outputWorkers.size(); i++){
        outputWorkers.at(i)->thread->start();
    }
    return 1;
}

int RecordService::review(){
    this->initInputWorker();
    for (int i = 0; i < inputWorkers.size(); i++){
        inputWorkers.at(i)->thread->start();
    }
    return 1;
}

int RecordService::stop(){
    for (int i = 0; i < inputWorkers.size(); i++){
        inputWorkers.at(i)->stop();
    }

    for (int i = 0; i < outputWorkers.size(); i++){
        outputWorkers.at(i)->stop();
    }

    for (int i = 0; i < inputWorkers.size(); i++){
        inputWorkers.at(i)->thread->wait();
    }

    for (int i = 0; i < outputWorkers.size(); i++){
        outputWorkers.at(i)->thread->wait();
    }

    for (int i = 0; i < outputWorkers.size(); i++){
        Stream* stream = outputWorkers.at(i)->stream;
        if (stream->mainStream){
            av_write_trailer(stream->format);
        }
    }

    for (int i = 0; i < outputWorkers.size(); i++){
        avcodec_close(outputWorkers.at(i)->stream->avStream->codec);
    }

    for (int i = 0; i < outputWorkers.size(); i++){
        Stream* stream = outputWorkers.at(i)->stream;
        if (stream->mainStream){
            if (!(stream->format->oformat->flags & AVFMT_NOFILE) && stream->format->pb) {
                if (avio_close(stream->format->pb) < 0) {
                    qErrnoWarning("Could not close output file");
                }
            }
            avformat_free_context(stream->format);
        }
    }

    this->inputWorkers.clear();
    this->outputWorkers.clear();
    this->bufferService->deleteAllBuffer();
    this->streamService->clearData();
    this->rightVideo->clearFrame();
    this->leftVideo->clearFrame();
    this->recordingView->clearSurface();
    return 1;
}

int RecordService::pause(){
    return -1;
}
