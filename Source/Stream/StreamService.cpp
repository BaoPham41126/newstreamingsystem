/***********************************************************************
 * Module:  StreamService.cpp
 * Author:  BaoPham
 * Modified: Wednesday, November 22, 2017 10:06:03 PM
 * Purpose: Implementation of the class StreamService
 ***********************************************************************/

#include "Header/Stream/StreamService.h"
#include <QDebug>
#include <QDate>
#include <QDir>

QList<Stream*> StreamService::createInputStreams(AVInfo* info) {
    QList<Stream*> streams;
    AVFormatContext *format = avformat_alloc_context();
    AVStream *stream;
    InputStream* result = new InputStream();

    //Input contains only 1 stream
    StreamInfo* streamInfo = info->streams.at(0);

    AVDictionary *options = NULL;
    AudioStreamInfo* audioInfo = qobject_cast<AudioStreamInfo*>(streamInfo);
    if (audioInfo != NULL){
        QString sampleRate = QString::number(audioInfo->sampleRate);
        av_dict_set(&options, "sample_rate", sampleRate.toStdString().c_str(), 0);
    }

    VideoStreamInfo* videoInfo = qobject_cast<VideoStreamInfo*>(streamInfo);
    if (videoInfo != NULL){
        QString optionString = QString::number(videoInfo->width) + "x" + QString::number(videoInfo->height);
        av_dict_set(&options, "video_size", optionString.toStdString().c_str(), 0);
    }

    AVInputFormat* iformat = av_find_input_format(info->driver.toStdString().c_str());

    int ret = avformat_open_input(&format, info->url.toStdString().c_str(), iformat, &options);
    if (ret < 0) {
        qErrnoWarning("Error while opening input");
        return streams;
    }

    ret = avformat_find_stream_info(format, NULL);
    av_dump_format(format, totalIn++, format->filename, 0);
    if (ret < 0) {
        avformat_close_input(&format);
        return streams;
    }

    if (audioInfo != NULL){
        for (int i = 0; i < format->nb_streams; i++) {
            if (format->streams[i]->codec->codec_type == AVMEDIA_TYPE_AUDIO) {
                stream = format->streams[i];
            }
        }
        stream->codec->bit_rate = audioInfo->bitRate;
        stream->codec->sample_fmt = audioInfo->sampleFormat;
        stream->codec->sample_rate = audioInfo->sampleRate;
        stream->time_base.num = 1;
        stream->time_base.den = audioInfo->sampleRate;
    } else {
        for (int i = 0; i < format->nb_streams; i++) {
            if (format->streams[i]->codec->codec_type == AVMEDIA_TYPE_VIDEO) {
                stream = format->streams[i];
            }
        }
        AVRational avg_frame_rate;
        avg_frame_rate.num = 1;
        avg_frame_rate.den = 10;
        stream->avg_frame_rate = avg_frame_rate;
    }

    result->infoId = streamInfo->id;
    result->format = format;
    result->avStream = stream;
    result->mutex = NULL;
    stream->codec = codecService->createInputCodecContext(result, streamInfo);
    streams.append(result);
    return streams;
}

Stream* StreamService::createOutputStream(AVFormatContext* format, QMutex* streamMutex, StreamInfo* info){
    OutputStream* result = new OutputStream();

    AVStream* stream = avformat_new_stream(format, NULL);
    AudioStreamInfo* audioInfo = qobject_cast<AudioStreamInfo*>(info);
    if (audioInfo != NULL){
        AVRational time_base;
        time_base.num = 1;
        time_base.den = audioInfo->sampleRate;
        stream->time_base = time_base;
    } else {
        VideoStreamInfo* videoInfo = qobject_cast<VideoStreamInfo*>(info);
        AVRational time_base;
        time_base.num = 1;
        time_base.den = videoInfo->frameRate;
        stream->time_base = time_base;
    }

    result->infoId = info->id;
    result->format = format;
    result->avStream = stream;
    result->mutex = streamMutex;
    result->queueSize = info->queueSize;
    stream->codec = codecService->createOutputCodecContext(result, info);
    return result;
}

QList<Stream*> StreamService::createOutputStreams(AVInfo* info) {
    QList<Stream*> streams;
    AVFormatContext* format;
    QMutex* streamMutex = new QMutex();
    QString url = info->url;
    if (url == NULL || url.isEmpty()){
        Class* currentClass = courseService->getCurrentClass();
        Subject* currentSubject = courseService->getSubject(currentClass->subjectId);
        if (info->outputType == HDD){
            QString dirPath = currentSubject->name + "/" +
                    currentClass->name +  "/" +
                    QDate::currentDate().toString();
            QDir dir;
            if (!dir.exists(dirPath)){
                dir.mkpath(dirPath);
            }
            QString sessionPath;
            if (currentSession == -1){
                int session = 0;
                do {
                    sessionPath = dirPath + "/session-" + QString::number(session++);
                } while (dir.exists(sessionPath));
                currentSession = session - 1;
            } else {
                sessionPath = dirPath + "/session-" + QString::number(currentSession);
            }
            dir.mkpath(sessionPath);
            url = sessionPath + "/" + info->name + "." + info->format;
        } else {
            url = "rtmp://a.rtmp.youtube.com/live2/5t6y-a99a-mesa-56hb";
        }
    }
    avformat_alloc_output_context2(
                &format,
                NULL,
                info->format.toStdString().c_str(),
                url.toStdString().c_str()
    );


    for (int i = 0; i <info->streams.size(); i++){
        streams.append(createOutputStream(format, streamMutex, info->streams.at(i)));
    }
    streams.at(0)->mainStream = true;

    if (!(format->flags & AVFMT_NOFILE)) {
        if (avio_open(&format->pb, url.toStdString().c_str(), AVIO_FLAG_WRITE) < 0) {
            qErrnoWarning("Error while opening output");
            return streams;
        }
    }

    avformat_write_header(format, NULL);
    av_dump_format(format, totalOut++, format->filename, 1);
    return streams;
}

QList<Stream*> StreamService::createStreams(AVInfo* info) {
    if (info->type == INPUT){
        return createInputStreams(info);
    } else {
        return createOutputStreams(info);
    }
}
