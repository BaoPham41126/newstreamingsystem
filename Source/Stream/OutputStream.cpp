/***********************************************************************
 * Module:  OutputStream.cpp
 * Author:  BaoPham
 * Modified: Wednesday, November 22, 2017 9:14:49 PM
 * Purpose: Implementation of the class OutputStream
 ***********************************************************************/

#include "Header/Stream/OutputStream.h"

int OutputStream::writePacket(AVPacket* packet) {
   return av_interleaved_write_frame(this->format, packet);
}
