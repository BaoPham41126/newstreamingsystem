/***********************************************************************
 * Module:  InputStream.cpp
 * Author:  BaoPham
 * Modified: Wednesday, November 22, 2017 9:14:45 PM
 * Purpose: Implementation of the class InputStream
 ***********************************************************************/

#include "Header/Stream/InputStream.h"

int InputStream::readPacket(AVPacket* packet) {
    return av_read_frame(this->format, packet);
}
