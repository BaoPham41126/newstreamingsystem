#include <Header/View/Recording.h>
#include <QCheckBox>
#include <QPushButton>
#include <QSignalMapper>
#include <QMessageBox>
#include <QHBoxLayout>

Recording::Recording(QWidget *parent) :
    QWidget(NULL),
    ui(new Ui::Recording)
{
    this->parentWidget = parent;
    ui->setupUi(this);
    QStringList _header;
    ui->output_tb->setColumnCount(3);
    _header << "Current Output" << "Active" << "Action";
    ui->output_tb->setHorizontalHeaderLabels(_header);
    ui->output_tb->setColumnWidth(0,300);
    ui->output_tb->setColumnWidth(1,60);
    ui->output_tb->setColumnWidth(2,180);
    ui->output_tb->setStyleSheet("QCheckBox::indicator:on  {image: url(':icon/Image/on');} QCheckBox::indicator:off {image: url(':icon/Image/off');}");

    recordService->leftVideo = new VideoSurface(ui->left_preview);
    recordService->rightVideo = new VideoSurface(ui->right_preview);
    recordService->setRecordView(this);

    this->setRecording(false);
    this->showOutputInfo();
}

void Recording::showOutputInfo(){
    QList<AVInfo*> outputs = outputService->getAllOutput();
    ui->output_tb->setRowCount(outputs.size());
    for (int i = 0; i < outputs.size(); i++){
        AVInfo* output = outputs.at(i);
        QTableWidgetItem* nameItem = new QTableWidgetItem(output->name);
        ui->output_tb->setItem(i, 0, nameItem);

        QCheckBox* activeCheckBox = new QCheckBox();
        activeCheckBox->setStyleSheet("margin-left:20%; margin-right:20%;");
        activeCheckBox->setChecked(output->active);
        connect(activeCheckBox, SIGNAL(toggled(bool)), output, SLOT(changeActiveState()));
        ui->output_tb->setCellWidget(i, 1, activeCheckBox);

        QPushButton* editButton = new QPushButton();
        editButton->setText("Edit");
        QSignalMapper* editSignalMapper = new QSignalMapper(this);
        connect(editButton, SIGNAL(clicked(bool)), editSignalMapper, SLOT(map()));
        editSignalMapper->setMapping(editButton, output->id);
        connect(editSignalMapper, SIGNAL(mapped(QString)), this, SLOT(editOutput(QString)));

        QPushButton* deleteButton = new QPushButton();
        deleteButton->setText("Delete");
        QSignalMapper* deleteSignalMapper = new QSignalMapper(this);
        connect(deleteButton, SIGNAL(clicked(bool)), deleteSignalMapper, SLOT(map()));
        deleteSignalMapper->setMapping(deleteButton, output->id);
        connect(deleteSignalMapper, SIGNAL(mapped(QString)), this, SLOT(deleteOutput(QString)));

        QHBoxLayout* actionLayout = new QHBoxLayout();
        actionLayout->setContentsMargins(0, 0, 0, 0);
        actionLayout->addWidget(editButton, 1);
        actionLayout->addWidget(deleteButton, 1);

        QWidget* widget = new QWidget();
        widget->setLayout(actionLayout);
        ui->output_tb->setCellWidget(i, 2, widget);
    }
}

void Recording::editOutput(QString id){
    NewOutputDialog *new_output = new NewOutputDialog(this, id);
    new_output->setWindowTitle("Edit Output");
    new_output->setModal(false);
    connect(new_output, &NewOutputDialog::saved, this, &Recording::showOutputInfo);
    new_output->exec();
}

void Recording::deleteOutput(QString id){
    QMessageBox::StandardButton reply;
    reply = QMessageBox::question(this, "Confirm", "Are you sure to delete this output?", QMessageBox::Yes|QMessageBox::No);
    if (reply == QMessageBox::Yes) {
        outputService->deleteOutput(id);
        this->showOutputInfo();
    }
}

Recording::~Recording()
{
    delete ui;
}

void Recording::on_qbtn_back_clicked()
{
    this->parentWidget->show();
    this->hide();
}

void Recording::setRecording(bool recording){
    ui->pause_btn->setEnabled(recording);
    ui->start_btn->setEnabled(!recording);
    ui->preview_btn->setEnabled(!recording);
}

void Recording::on_start_btn_clicked()
{
    this->recordService->start();
    this->setRecording(true);
}

void Recording::on_pause_btn_clicked()
{
    this->recordService->stop();
    this->setRecording(false);
}

void Recording::on_stop_btn_clicked()
{
    this->recordService->stop();
    this->setRecording(false);
}

void Recording::on_preview_btn_clicked()
{
    this->recordService->review();
    this->setRecording(true);
}

void Recording::on_btn_add_clicked()
{
    NewOutputDialog *new_output = new NewOutputDialog(this);
    new_output->setWindowTitle("New Output");
    new_output->setModal(false);
    connect(new_output, &NewOutputDialog::saved, this, &Recording::showOutputInfo);
    new_output->exec();
}

AudioSurface* Recording::addNewAudioSurface(QString name)
{
    QListWidgetItem* widgetItem = new QListWidgetItem(ui->audio_preview_list);
    QWidget* widget = new QWidget();
    QHBoxLayout* layout = new QHBoxLayout();
    layout->setContentsMargins(0, 0, 0, 0);
    QLabel* label = new QLabel(name);
    layout->addWidget(label, 1);
    QProgressBar* progressBar = new QProgressBar();
    layout->addWidget(progressBar, 2);
    layout->setSizeConstraint(QLayout::SetFixedSize);
    widget->setLayout(layout);
    widgetItem->setSizeHint(widget->sizeHint());
    this->ui->audio_preview_list->addItem(widgetItem);
    this->ui->audio_preview_list->setItemWidget(widgetItem, widget);

    AudioSurface* surface = new AudioSurface(progressBar);
    return surface;
}

void Recording::clearSurface(){
    ui->audio_preview_list->clear();
//    while (ui->audio_preview_list->count() > 0){
//        ui->audio_preview_list->removeItemWidget(ui->audio_preview_list->item(0));
//    }
}
