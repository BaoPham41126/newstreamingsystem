﻿#include "Header/View/Home.h"

Home::Home(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::Home)
{
    ui->setupUi(this);
}

Home::~Home()
{
    delete ui;
}

void Home::on_pbtn_quit_clicked()
{
    qInfo("Quit");
    this->close();
}

void Home::on_pbtn_recording_clicked()
{
    if (this->recording == NULL){
        this->recording = new Recording(this);
        this->recording->setWindowTitle("BK Elearning");
    }
    this->recording->show();
    this->hide();
}

void Home::on_pbtn_course_clicked()
{
    if (this->course == NULL){
        this->course = new Course(this);
        this->course->setWindowTitle("BK Elearning");
    }
    this->course->show();
    this->hide();
}
