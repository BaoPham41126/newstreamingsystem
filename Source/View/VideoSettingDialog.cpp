#include "Header/View/VideoSettingDialog.h"
#include "ui_VideoSettingDialog.h"

QString videoCodec2String(AVCodecID codecId){
    switch (codecId) {
    case AV_CODEC_ID_H264:
        return "H264";
    default:
        break;
    }
    return NULL;
}

AVCodecID videoString2Codec(QString str){
    if (str == "H264"){
        return AV_CODEC_ID_H264;
    }
    return AV_CODEC_ID_NONE;
}

VideoSettingDialog::VideoSettingDialog(VideoStreamInfo* tmpVideoStreamInfo, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::VideoSettingDialog)
{
    ui->setupUi(this);
    this->tmpVideoStreamInfo = tmpVideoStreamInfo;
    this->showInfo();
}

VideoSettingDialog::~VideoSettingDialog()
{
    delete ui;
}

void VideoSettingDialog::showInfo()
{
    ui->video_bitrate->setText(QString::number(tmpVideoStreamInfo->bitRate));
    ui->video_frame_rate->setText(QString::number(tmpVideoStreamInfo->frameRate));
    ui->video_group_size->setText(QString::number(tmpVideoStreamInfo->GOPSize));
    ui->video_height->setText(QString::number(tmpVideoStreamInfo->height));
    ui->video_max_buffer_rate->setText(QString::number(tmpVideoStreamInfo->maxBufRate));
    ui->video_queue_size->setText(QString::number(tmpVideoStreamInfo->queueSize));
    ui->video_width->setText(QString::number(tmpVideoStreamInfo->width));
    ui->video_codec->addItem("H264");
    ui->video_codec->setCurrentText(videoCodec2String(tmpVideoStreamInfo->codecID));
    ui->video_pixel_format->addItem("YUV420");
}

void VideoSettingDialog::on_buttonBox_accepted()
{
    tmpVideoStreamInfo->bitRate = ui->video_bitrate->text().toInt();
    tmpVideoStreamInfo->frameRate = ui->video_frame_rate->text().toInt();
    tmpVideoStreamInfo->GOPSize = ui->video_group_size->text().toInt();
    tmpVideoStreamInfo->height = ui->video_height->text().toInt();
    tmpVideoStreamInfo->maxBufRate = ui->video_max_buffer_rate->text().toInt();
    tmpVideoStreamInfo->queueSize = ui->video_queue_size->text().toInt();
    tmpVideoStreamInfo->width = ui->video_width->text().toInt();
    tmpVideoStreamInfo->codecID = videoString2Codec(ui->video_codec->currentText());

    emit settingChanged(this->tmpVideoStreamInfo);
    this->hide();
}
