#include "Header/View/AddInputDialog.h"
#include <QtWidgets>

NewInputDialog::NewInputDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::NewInputDialog)
{
    ui->setupUi(this);
    QList<AVInfo*> inputVideo = inputService->getAvaitableVideo(true);
    QList<AVInfo*> outputAudio = inputService->getAvaitableAudio(true);
    for (int i = 0; i < inputVideo.size(); i++){
        QListWidgetItem* item = new QListWidgetItem(ui->listWidget);
        item->setData(Qt::UserRole, inputVideo.at(i)->streams.at(0)->id);
        item->setText("Video: " + inputVideo.at(i)->name);
        item->setCheckState(Qt::CheckState::Unchecked);
        ui->listWidget->addItem(item);
    }
    for (int i = 0; i < outputAudio.size(); i++){
        QListWidgetItem* item = new QListWidgetItem(ui->listWidget);
        item->setData(Qt::UserRole, outputAudio.at(i)->streams.at(0)->id);
        item->setText("Audio: " + outputAudio.at(i)->name);
        item->setCheckState(Qt::CheckState::Unchecked);
        ui->listWidget->addItem(item);
    }
}

NewInputDialog::~NewInputDialog()
{
    delete ui;
}

void NewInputDialog::on_buttonBox_accepted()
{
    QList<StreamInfo*> result;
    for(int i = 0; i < ui->listWidget->count(); i++){
        QListWidgetItem* item = ui->listWidget->item(i);
        if (item->checkState() != Qt::CheckState::Unchecked){
            if (item->text().startsWith("Video")){
                VideoStreamInfo* videoStream = new VideoStreamInfo();
                videoStream->inputId = item->data(Qt::UserRole).toString();
                videoStream->name = item->text();
                result.append(videoStream);
            } else {
                AudioStreamInfo* audioStream = new AudioStreamInfo();
                audioStream->inputId = item->data(Qt::UserRole).toString();
                audioStream->name = item->text();
                result.append(audioStream);
            }
        }
    }
    emit onAccepted(result);
    this->hide();
}
