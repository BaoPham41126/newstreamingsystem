#include "Header/View/Course.h"
#include "Header/View/Home.h"
#include <QInputDialog>
#include <QMessageBox>

Course::Course(QWidget *parent) :
    QWidget(NULL),
    ui(new Ui::Course)
{
    this->parentWidget = parent;
    ui->setupUi(this);
    loadData();
}

Course::~Course()
{
    delete ui;
}

void Course::on_pbtn_save_clicked()
{
    QString classId = ui->class_list->currentData().toString();
    if (classId != NULL){
        this->courceService->setCurrentClass(this->courceService->getClass(classId));
    }
    on_pbtn_cancel_clicked();
}

void Course::loadData(){
    subjects = courceService->getAllSubject();
    ui->cource_list->clear();
    for (int i = 0; i < subjects.size(); i++){
        ui->cource_list->addItem(subjects.at(i)->name, QVariant(subjects.at(i)->id));
    }
    Class* currentClass = this->courceService->getCurrentClass();
    Subject* currentCource;
    if (currentClass != NULL){
        currentCource = courceService->getSubject(currentClass->subjectId);

        ui->cource_list->setCurrentText(currentCource->name);
        ui->class_list->setCurrentText(currentClass->name);
    } else {
        if (subjects.size() > 0){
            ui->cource_list->setCurrentIndex(0);
        }
        if (classes.size() > 0){
            ui->class_list->setCurrentIndex(0);
        }
    }
}

void Course::on_cource_list_currentIndexChanged(int index)
{
    classes = courceService->getClassOfSubject(ui->cource_list->currentData().toString());
    ui->class_list->clear();
    for (int i = 0; i < classes.size(); i++){
        ui->class_list->addItem(classes.at(i)->name, classes.at(i)->id);
    }
}

void Course::on_insert_course_clicked()
{
    bool ok;
    QString newName = QInputDialog::getText(this, QString("New Course"), QString("Name"), QLineEdit::Normal, QString(""), &ok);
    if (ok){
        Subject* newSubject = new Subject();
        newSubject->name = newName;
        courceService->saveSubject(newSubject);
        this->loadData();
        ui->cource_list->setCurrentText(newName);
    }
}

void Course::on_insert_class_clicked()
{
    bool ok;
    QString courseName = ui->cource_list->currentText();
    QString newName = QInputDialog::getText(this, QString("New Class"), QString("Name"), QLineEdit::Normal, QString(""), &ok);
    if (ok){
        Class* newClass = new Class();
        newClass->name = newName;
        newClass->subjectId = ui->cource_list->currentData().toString();
        courceService->saveClass(newClass);
        this->loadData();
        ui->cource_list->setCurrentText(courseName);
        ui->class_list->setCurrentText(newName);
    }
}

void Course::on_pbtn_cancel_clicked()
{
    this->parentWidget->show();
    this->hide();
}

void Course::on_remove_course_clicked()
{
    QMessageBox::StandardButton reply;
    reply = QMessageBox::question(this, "Confirm", "Are you sure to delete this course?", QMessageBox::Yes|QMessageBox::No);
    if (reply == QMessageBox::Yes) {
        courceService->deleteSubject(ui->cource_list->currentData().toString());
    }
}

void Course::on_remove_class_clicked()
{
    QMessageBox::StandardButton reply;
    reply = QMessageBox::question(this, "Confirm", "Are you sure to delete this class?", QMessageBox::Yes|QMessageBox::No);
    if (reply == QMessageBox::Yes) {
        courceService->deleteClass(ui->class_list->currentData().toString());
    }
}
