#include "Header/View/AudioSettingDialog.h"
#include "ui_AudioSettingDialog.h"

QString audioCodec2String(AVCodecID codecId){
    switch (codecId) {
    case AV_CODEC_ID_AAC:
        return "AAC";
    case AV_CODEC_ID_MP3:
        return "MP3";
    default:
        break;
    }
    return NULL;
}

AVCodecID audioString2Codec(QString str){
    if (str == "AAC"){
        return AV_CODEC_ID_AAC;
    } else if (str == "MP3"){
        return AV_CODEC_ID_MP3;
    }
    return AV_CODEC_ID_NONE;
}

AudioSettingDialog::AudioSettingDialog(AudioStreamInfo* tmpAudioStreamInfo, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::AudioSettingDialog)
{
    ui->setupUi(this);
    this->tmpAudioStreamInfo = tmpAudioStreamInfo;
    this->showInfo();
}

AudioSettingDialog::~AudioSettingDialog()
{
    delete ui;
}

void AudioSettingDialog::showInfo()
{
    ui->audio_bitrate->setText(QString::number(tmpAudioStreamInfo->bitRate));
    ui->audio_numchanel->setText(QString::number(tmpAudioStreamInfo->numChanel));
    ui->audio_queue_size->setText(QString::number(tmpAudioStreamInfo->queueSize));
    ui->audio_sample_rate->setText(QString::number(tmpAudioStreamInfo->sampleRate));
    ui->audio_codec->addItem("AAC");
    ui->audio_codec->addItem("MP3");
    ui->audio_codec->setCurrentText(audioCodec2String(tmpAudioStreamInfo->codecID));
}

void AudioSettingDialog::on_buttonBox_accepted()
{
    tmpAudioStreamInfo->bitRate = ui->audio_bitrate->text().toInt();
    tmpAudioStreamInfo->numChanel = ui->audio_numchanel->text().toInt();
    tmpAudioStreamInfo->queueSize = ui->audio_queue_size->text().toInt();
    tmpAudioStreamInfo->sampleRate = ui->audio_sample_rate->text().toInt();
    tmpAudioStreamInfo->codecID = audioString2Codec(ui->audio_codec->currentText());

    emit settingChanged(this->tmpAudioStreamInfo);
    this->hide();
}
