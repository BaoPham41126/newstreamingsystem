#include "Header/View/NewOutputDialog.h"

NewOutputDialog::NewOutputDialog(QWidget *parent, QString outputId) :
    QDialog(parent),
    ui(new Ui::NewOutputDialog)
{
    ui->setupUi(this);

    ui->tableWidget->setColumnCount(2);
    QStringList _header;
    _header << "Current Input" << "Action";
    ui->tableWidget->setHorizontalHeaderLabels(_header);
    ui->tableWidget->setColumnWidth(0, 300);
    ui->tableWidget->setColumnWidth(1, 174);

    if (!outputId.isEmpty()){
        this->currentOutput = outputService->getOutputById(outputId)->clone();
    } else {
        this->currentOutput = new AVInfo();
    }
    this->showInfo();
}

void NewOutputDialog::showInfo(){
    ui->output_name->setText(this->currentOutput->name);
    switch (this->currentOutput->outputType) {
    case LIVE:
        ui->rbtn_live->click();
        break;
    case HDD:
        ui->rbtn_hdd->click();
        break;
    case YOUTUBE:
        ui->rbtn_youtube->click();
        break;
    default:
        break;
    }
    ui->tableWidget->setRowCount(currentOutput->streams.size());
    for (int i = 0; i < currentOutput->streams.size(); i++){
        StreamInfo* stream = currentOutput->streams.at(i);
        QTableWidgetItem *item = new QTableWidgetItem(stream->name);
        ui->tableWidget->setItem(i, 0, item);

        QPushButton* deleteButton = new QPushButton();
        deleteButton->setText("Delete");
        QSignalMapper* deleteSignalMapper = new QSignalMapper(this);
        connect(deleteButton, SIGNAL(clicked(bool)), deleteSignalMapper, SLOT(map()));
        deleteSignalMapper->setMapping(deleteButton, stream->id);
        connect(deleteSignalMapper, SIGNAL(mapped(QString)), this, SLOT(deleteStream(QString)));

        QPushButton* editButton = new QPushButton();
        editButton->setText("Edit");
        QSignalMapper* editSignalMapper = new QSignalMapper(this);
        connect(editButton, SIGNAL(clicked(bool)), editSignalMapper, SLOT(map()));
        editSignalMapper->setMapping(editButton, stream->id);
        connect(editSignalMapper, SIGNAL(mapped(QString)), this, SLOT(editStream(QString)));

        QHBoxLayout* actionLayout = new QHBoxLayout();
        actionLayout->setContentsMargins(0, 0, 0, 0);
        actionLayout->addWidget(editButton, 1);
        actionLayout->addWidget(deleteButton, 1);

        QWidget* widget = new QWidget();
        widget->setLayout(actionLayout);
        ui->tableWidget->setCellWidget(i, 1, widget);
    }
    ui->tableWidget->repaint();
    ui->tableWidget->update();
}

NewOutputDialog::~NewOutputDialog()
{
    delete ui;
}

void NewOutputDialog::onSettingChanged(StreamInfo* streamInfo){
    StreamInfo* info = this->currentOutput->getStreamById(streamInfo->id);
    info->copyFormatOf(streamInfo);
}

void NewOutputDialog::on_confirm_accepted()
{
    currentOutput->active = true;
    currentOutput->type = OUTPUT;
    currentOutput->name = ui->output_name->text();
    outputService->saveOutput(currentOutput);
    for (int i = 0; i < currentOutput->streams.size(); i++){
        mapperService->bindInputOutput(currentOutput->streams.at(i)->inputId, currentOutput->streams.at(i)->id);
    }
    for (int i = 0; i < this->deleteStreamIds.size(); i++){
        mapperService->unbindInputOutput(deleteStreamIds.at(i));
    }
    emit saved();
}

void NewOutputDialog::on_add_input_clicked()
{
    inputDialog = new NewInputDialog(this);
    inputDialog->setModal(true);
    connect(inputDialog, &NewInputDialog::onAccepted, this, &NewOutputDialog::onInputAdded);
    inputDialog->exec();
}

void NewOutputDialog::onInputAdded(QList<StreamInfo *> streams){
    for (int i = 0; i < streams.size(); i++){
        this->currentOutput->streams.append(streams.at(i));
    }
    showInfo();
}

void NewOutputDialog::on_output_name_textChanged(const QString &arg1)
{
    this->currentOutput->name = arg1;
}

void NewOutputDialog::on_rbtn_hdd_clicked()
{
    this->currentOutput->outputType = HDD;
    ui->groupBox->show();
    ui->groupBox->setTitle("HDD Configuration");
    ui->config_name->setText("Format");
    ui->use_config->setText(this->currentOutput->format);
}

void NewOutputDialog::on_rbtn_live_clicked()
{
    this->currentOutput->outputType = LIVE;
    this->currentOutput->format = "flv";
    ui->groupBox->show();
    ui->groupBox->setTitle("LIVE Configuration");
    ui->config_name->setText("URL");
    ui->use_config->setText(this->currentOutput->url);
}

void NewOutputDialog::on_rbtn_youtube_clicked()
{
    this->currentOutput->outputType = YOUTUBE;
    this->currentOutput->format = "flv";
    ui->groupBox->hide();
}

void NewOutputDialog::on_use_config_textChanged(const QString &arg1)
{
    if (this->currentOutput->outputType == LIVE){
        this->currentOutput->url = arg1;
    } else if (this->currentOutput->outputType == HDD){
        this->currentOutput->format = arg1;
    }
}

void NewOutputDialog::deleteStream(QString id){
    this->currentOutput->removeStreamById(id);
    this->deleteStreamIds.append(id);
    this->showInfo();
}

void NewOutputDialog::editStream(QString id){
    StreamInfo* streamInfo = this->currentOutput->getStreamById(id);
    if (streamInfo->streamType == VIDEO){
        VideoStreamInfo* tmpVideoStreamInfo = new VideoStreamInfo();
        tmpVideoStreamInfo->copyFormatOf(streamInfo);
        tmpVideoStreamInfo->id = streamInfo->id;
        VideoSettingDialog *outputSettingDialog = new VideoSettingDialog(tmpVideoStreamInfo, this);
        outputSettingDialog->setWindowTitle("Video Setting");
        outputSettingDialog->setModal(false);
        connect(outputSettingDialog, SIGNAL(settingChanged(StreamInfo*)), this, SLOT(onSettingChanged(StreamInfo*)));
        outputSettingDialog->exec();
    } else {
        AudioStreamInfo* tmpAudioStreamInfo = new AudioStreamInfo();
        tmpAudioStreamInfo->copyFormatOf(streamInfo);
        tmpAudioStreamInfo->id = streamInfo->id;
        AudioSettingDialog *outputSettingDialog = new AudioSettingDialog(tmpAudioStreamInfo, this);
        outputSettingDialog->setWindowTitle("Audio Setting");
        outputSettingDialog->setModal(false);
        connect(outputSettingDialog, SIGNAL(settingChanged(StreamInfo*)), this, SLOT(onSettingChanged(StreamInfo*)));
        outputSettingDialog->exec();
    }
}
