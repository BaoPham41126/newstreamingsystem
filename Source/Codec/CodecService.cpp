/***********************************************************************
 * Module:  CodecService.cpp
 * Author:  BaoPham
 * Modified: Wednesday, November 22, 2017 10:10:50 PM
 * Purpose: Implementation of the class CodecService
 ***********************************************************************/

#include "Header/Codec/CodecService.h"
#include "iostream"
AVCodecContext* CodecService::createOutputCodecContext(Stream *stream, StreamInfo* info){
    VideoStreamInfo* videoInfo = qobject_cast<VideoStreamInfo*>(info);
    if (videoInfo != NULL){
        return this->_createVideoCodecContext(stream, videoInfo);
    }
    AudioStreamInfo* audioInfo = qobject_cast<AudioStreamInfo*>(info);
    if (audioInfo != NULL){
        return this->_createAudioCodecContext(stream, audioInfo);
    }
    return NULL;
}

AVCodecContext* CodecService::_createVideoCodecContext(Stream *stream, VideoStreamInfo* info){
    AVCodecContext *result;
    AVCodec *codec = avcodec_find_encoder(info->codecID);
    AVDictionary * options = NULL;

    if (!codec) {
        qErrnoWarning("Codec not found");
        return NULL;
    }

    result = avcodec_alloc_context3(codec);

    //set video stream value
    result->thread_count = 1;
    result->codec_id = info->codecID;
    result->time_base.den = info->frameRate;
    result->time_base.num = 1;
    result->gop_size = info->GOPSize;
    result->pix_fmt = info->pixelFormat;
    result->height = info->height;
    result->width = info->width;
    result->bit_rate = info->bitRate;
    result->codec_type = AVMediaType::AVMEDIA_TYPE_VIDEO;

    QString bitRateOption = QString::number(info->bitRate);
    //set dictionary value for output stream
    av_dict_set(&options, "b:v", bitRateOption.toStdString().c_str(), 0);
    if (info->codecID == AV_CODEC_ID_H264){
        av_opt_set(result->priv_data, "tune", "zerolatency", 0);
    }
    if(info->baseLine == 1){
        av_opt_set(result->priv_data, "profile", "baseline", AV_OPT_SEARCH_CHILDREN);
    }

    // some formats want stream headers to be separate
    if (stream->format->oformat->flags & AVFMT_GLOBALHEADER)
        result->flags |= CODEC_FLAG_GLOBAL_HEADER;
    if (codec->capabilities & CODEC_CAP_TRUNCATED)
        result->flags |= CODEC_FLAG_TRUNCATED; // we do not send complete frames

    //open output stream to write data
    if (avcodec_open2(result, codec, &options) < 0) {
        qErrnoWarning("Error while opening codec");
        return NULL;
    }

    // set codec
    result->codec = codec;
    stream->avStream->codec = result;
    return result;
}

AVCodecContext* CodecService::_createAudioCodecContext(Stream *stream, AudioStreamInfo* info){
    AVCodecContext *result = stream->avStream->codec;
    result->codec_id = info->codecID;
    AVCodec *codec;

    AVDictionary * options = NULL;

    if(result->codec_id == AV_CODEC_ID_AAC)
        result->profile = FF_PROFILE_AAC_MAIN;
    result->sample_fmt = info->sampleFormat;
    result->sample_rate = info->sampleRate;
    result->channels = info->numChanel;
    result->bit_rate = info->bitRate;
    result->codec_type = AVMediaType::AVMEDIA_TYPE_AUDIO;

    // some formats want stream headers to be separate
    if (stream->format->oformat->flags & AVFMT_GLOBALHEADER)
        result->flags |= CODEC_FLAG_GLOBAL_HEADER;

    // find the audio encoder
    codec = avcodec_find_encoder(result->codec_id);
    if (!codec) {
        qErrnoWarning("Codec not found");
        return NULL;
    }

    // set channel layout
    if(result->codec_id == AV_CODEC_ID_AAC){
        result->channel_layout = AV_CH_LAYOUT_STEREO;
    } else {
        result->channel_layout = AV_CH_LAYOUT_MONO;
    }

    //get channel
    result->channels  = av_get_channel_layout_nb_channels(result->channel_layout);
    result->sample_fmt  = codec->sample_fmts ? codec->sample_fmts[0]: AV_SAMPLE_FMT_FLTP;

    //open output
    if (avcodec_open2(result, codec, &options) < 0) {
        qErrnoWarning("Error while opening codec");
        return NULL;
    }

    //set codec and return audio stream
    result->codec = codec;
    return result;
}

AVCodecContext* CodecService::createInputCodecContext(Stream *stream, StreamInfo* info){
    // get video stream id
    AVDictionary *options = NULL;

    //find decoder for video stream
    AVCodec* icodec = avcodec_find_decoder(stream->avStream->codec->codec_id);
    if (!icodec) {
        qErrnoWarning("Codec not found");
        return NULL;
    }

    //open device
    if (avcodec_open2(stream->avStream->codec, icodec, &options) < 0) {
        qErrnoWarning("Error while opening codec");
        return NULL;
    }
    stream->avStream->codec->codec = icodec;
    return stream->avStream->codec;
}
