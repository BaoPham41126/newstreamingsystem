/***********************************************************************
 * Module:  InputService.cpp
 * Author:  BaoPham
 * Modified: Wednesday, November 22, 2017 10:21:17 PM
 * Purpose: Implementation of the class InputService
 ***********************************************************************/

#include "Header/InputOutput/InputService.h"

AVInfo* InputService::saveInput(AVInfo* info){
    if (info->id == NULL){
        info->id = QUuid::createUuid().toString();
    }
    for (int i = 0; i < info->streams.size(); i++){
        if (info->streams.at(i)->id == NULL){
            info->streams.at(i)->id = QUuid::createUuid().toString();
        }
    }
    int inputIndex = getInputIndex(info->id);
    if (inputIndex != -1){
        inputs.removeAt(inputIndex);
    }
    inputs.append(info);
    this->saveData();
    return info;
}

StreamInfo* InputService::getStreamById(QString id){
    for (int i = 0; i < inputs.size(); i++){
        for (int j = 0; j < inputs.at(i)->streams.size(); j++){
            if (inputs.at(i)->streams.at(j)->id == id){
                return inputs.at(i)->streams.at(j);
            }
        }
    }
    return NULL;
}

AVInfo* InputService::getInputByURL(QString url){
    for (int i = 0; i < inputs.size(); i++){
        if (inputs.at(i)->url == url){
            return inputs.at(i);
        }
    }
    return NULL;
}

void InputService::getInputResolution(QString deviceName, int* width, int* height){
    QProcess *proc_ovpn = new QProcess(this);
    proc_ovpn->start("v4l2-ctl --list-formats-ext --device=" + deviceName);
    proc_ovpn->waitForFinished();
    QString listSizeString(proc_ovpn->readAllStandardOutput());
    QRegExp rx("(\\d+x\\d+)");
    rx.indexIn(listSizeString);
    QStringList list = rx.capturedTexts();
    QString maxResolution = list.at(0);
    QString widthString = maxResolution.mid(0, maxResolution.indexOf("x"));
    QString heightString = maxResolution.mid(maxResolution.indexOf("x") + 1);
    *width = widthString.toInt();
    *height = heightString.toInt();
}

QList<AVInfo*> InputService::getAvaitableVideo(bool reload){
    static QString rootDir = "/sys/class/video4linux";
    QList<AVInfo*> result;
    QDirIterator dirIterator(rootDir, QDir::Dirs | QDir::NoDotAndDotDot);
    //Video
    while (dirIterator.hasNext()) {
        QString inputDir = dirIterator.next();
        inputDir = inputDir.mid(inputDir.lastIndexOf("/") + 1);
        QString url = "/dev/" + inputDir;
        AVInfo* info = this->getInputByURL(url);
        bool isNew = false;
        if (info == NULL){
            reload = true;
            isNew = true;
            info = new AVInfo();
            info->type = INPUT;
            info->url = url;
            QFile nameFile(rootDir + "/" + inputDir + "/name");
            if (nameFile.open(QIODevice::ReadOnly)){
               QTextStream in(&nameFile);
               info->name = in.readLine();
               nameFile.close();
            }
            info->driver = "video4linux";
        }
        if (reload){
            VideoStreamInfo* stream;
            if (isNew){
                stream = new VideoStreamInfo();
            } else {
                stream = (VideoStreamInfo*) info->streams.at(0);
            }
            getInputResolution(url, &(stream->width), &(stream->height));
            info->streams.clear();
            info->streams.append(stream);
            this->saveInput(info);
        }
        result.append(info);
    }

    //Screen
    AVInfo* screenInfo = this->getInputByURL(":0.0");
    bool isNew = false;
    if (screenInfo == NULL){
        reload = true;
        isNew = true;
        screenInfo = new AVInfo();
        screenInfo->url =":0.0";
        screenInfo->type = INPUT;
        screenInfo->driver = "x11grab";
        screenInfo->name = "Screen";
    }
    if(reload){
        VideoStreamInfo* stream;
        if (isNew){
            stream = new VideoStreamInfo();
        } else {
            stream = (VideoStreamInfo*) screenInfo->streams.at(0);
        }
        QScreen *screen = QGuiApplication::primaryScreen();
        QRect  screenGeometry = screen->geometry();
        stream->height = screenGeometry.height();
        stream->width = screenGeometry.width();
        screenInfo->streams.clear();
        screenInfo->streams.append(stream);
        this->saveInput(screenInfo);
    }
    result.append(screenInfo);
    return result;
}

QList<AVInfo*> InputService::getAvaitableAudio(bool reload){
    static QString rootDir = "/proc/asound";
    QDirIterator dirIterator(rootDir, QStringList() << "card*", QDir::Dirs | QDir::NoDotAndDotDot);
    QList<AVInfo*> result;
    while (dirIterator.hasNext()) {
        QString inputDir = dirIterator.next();
        inputDir = inputDir.mid(inputDir.lastIndexOf("/") + 1);
        QString url;
        QFile nameFile(rootDir + "/" + inputDir + "/id");
        if (nameFile.open(QIODevice::ReadOnly)){
           QTextStream in(&nameFile);
           url = "plughw:CARD=" + in.readLine();
           nameFile.close();
        }
        AVInfo* info = this->getInputByURL(url);
        bool isNew = false;
        if (info == NULL){
            reload = true;
            isNew = true;
            info = new AVInfo();
            info->type = INPUT;
            info->url = url;
            info->name = url;
            info->driver = "alsa";
        }
        if (reload){
            AudioStreamInfo* stream;
            if (isNew){
                stream = new AudioStreamInfo();
            } else {
                stream = (AudioStreamInfo*) info->streams.at(0);
            }
            stream->sampleRate = 44100;
            info->streams.clear();
            info->streams.append(stream);
            this->saveInput(info);
        }
        result.append(info);
    }
    return result;
}

QList<AVInfo*> InputService::getAllInput(){
    return inputs;
}

void InputService::deleteInput(QString id){
    int inputIndex = getInputIndex(id);
    if (inputIndex != -1){
        inputs.removeAt(inputIndex);
    }
}

int InputService::getInputIndex(QString id){
    for (int i = 0; i < this->inputs.size(); i++){
        if (this->inputs.at(i)->id == id){
            return i;
        }
    }
    return -1;
}

void InputService::reloadInputFormat(){
    this->getAvaitableVideo(true);
    this->getAvaitableAudio(true);
}

void InputService::loadData(){
    QFile* dataFile = new QFile(this->dataFileString);
    if (dataFile->open(QIODevice::ReadWrite)){
        QDataStream dataStream(dataFile);
        int num;
        dataStream >> num;
        for (int i = 0; i < num; i++){
            AVInfo* info = new AVInfo();
            dataStream >> *info;
            this->inputs.append(info);
        }
        dataFile->close();
    }
}

void InputService::saveData(){
    QFile* dataFile = new QFile(this->dataFileString);
    if (dataFile->open(QIODevice::ReadWrite)){
        QDataStream dataStream(dataFile);
        dataStream << this->inputs.size();
        for (int i = 0; i < this->inputs.size(); i++){
            dataStream << *(this->inputs.at(i));
        }
        dataFile->close();
    }
}
