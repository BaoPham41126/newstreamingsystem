/***********************************************************************
 * Module:  InputOutputMapperService.cpp
 * Author:  BaoPham
 * Modified: Friday, November 24, 2017 1:14:39 AM
 * Purpose: Implementation of the class InputOutputMapperService
 ***********************************************************************/

#include "Header/InputOutput/InputOutputMapperService.h"

QList<StreamInfo*> InputOutputMapperService::getInputOfOutput(QString outputId) {
    QList<StreamInfo*> result;
    for (int i = 0; i < inputOutputs.size(); i++){
        if (inputOutputs.at(i)->outputId == outputId){
            StreamInfo* input = inputService->getStreamById(inputOutputs.at(i)->inputId);
            result.append(input);
        }
    }
    return result;
}

QList<StreamInfo*> InputOutputMapperService::getOutputOfInput(QString inputId) {
    QList<StreamInfo*> result;
    for (int i = 0; i < inputOutputs.size(); i++){
        if (inputOutputs.at(i)->inputId == inputId){
            StreamInfo* output = outputService->getActiveStreamById(inputOutputs.at(i)->outputId);
            if (output != NULL){
                result.append(output);
            }
        }
    }
    return result;
}

InputOutput* InputOutputMapperService::bindInputOutput(QString inputId, QString outputId) {
    InputOutput* inputOutput = new InputOutput(inputId, outputId);
    this->inputOutputs.append(inputOutput);
    this->saveData();
    return inputOutput;
}

void InputOutputMapperService::unbindInputOutput(QString outputId) {
    for (int i = 0; i < inputOutputs.size(); i++){
        if (inputOutputs.at(i)->outputId == outputId){
            inputOutputs.removeAt(i);
            this->saveData();
            break;
        }
    }
}

void InputOutputMapperService::loadData(){
    QFile* dataFile = new QFile(this->dataFileString);
    if (dataFile->open(QIODevice::ReadWrite)){
        QDataStream dataStream(dataFile);
        int num;
        dataStream >> num;
        for (int i = 0; i < num; i++){
            InputOutput* entry = new InputOutput();
            dataStream >> *entry;
            this->inputOutputs.append(entry);
        }
        dataFile->close();
    }
}

void InputOutputMapperService::saveData(){
    QFile* dataFile = new QFile(this->dataFileString);
    if (dataFile->open(QIODevice::ReadWrite)){
        QDataStream dataStream(dataFile);
        dataStream << this->inputOutputs.size();
        for (int i = 0; i < this->inputOutputs.size(); i++){
            dataStream << *(this->inputOutputs.at(i));
        }
        dataFile->close();
    }
}
