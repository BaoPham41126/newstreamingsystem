/***********************************************************************
 * Module:  OutputService.cpp
 * Author:  BaoPham
 * Modified: Wednesday, November 22, 2017 10:21:24 PM
 * Purpose: Implementation of the class OutputService
 ***********************************************************************/

#include "Header/InputOutput/OutputService.h"
#include <QtAlgorithms>

AVInfo* OutputService::saveOutput(AVInfo* info){
    if (info->id == NULL){
        QUuid uuid = QUuid::createUuid();
        info->id = uuid.toString();
    }
    for (int i = 0; i < info->streams.size(); i++){
        if (info->streams.at(i)->id == NULL){
            info->streams.at(i)->id = QUuid::createUuid().toString();
        }
    }
    int outputIndex = getOutputIndex(info->id);
    if (outputIndex != -1){
        outputs.removeAt(outputIndex);
    }
    outputs.append(info);
    this->saveData();
    return info;
}

StreamInfo* OutputService::getActiveStreamById(QString id){
    for (int i = 0; i < outputs.size(); i++){
        for (int j = 0; j < outputs.at(i)->streams.size(); j++){
            if (outputs.at(i)->active && outputs.at(i)->streams.at(j)->id == id){
                return outputs.at(i)->streams.at(j);
            }
        }
    }
    return NULL;
}

AVInfo* OutputService::getOutputById(QString id){
    return this->outputs.at(getOutputIndex(id));
}

QList<AVInfo*> OutputService::getAllActiveOutput(){
    qSort(outputs.begin(), outputs.end(), AVInfo::lessThan);
    QList<AVInfo*> activeOutputs;
    for (int i = 0; i < outputs.size(); i++){
        if (outputs.at(i)->active){
            activeOutputs.append(outputs.at(i));
        }
    }
    return activeOutputs;
}

QList<AVInfo*> OutputService::getAllOutput(){
    qSort(outputs.begin(), outputs.end(), AVInfo::lessThan);
    return outputs;
}

void OutputService::deleteOutput(QString id){
    int outputIndex = getOutputIndex(id);
    if (outputIndex != -1){
        outputs.removeAt(outputIndex);
    }
}

int OutputService::getOutputIndex(QString id){
    for (int i = 0; i < outputs.size(); i++){
        if (outputs.at(i)->id == id){
            return i;
        }
    }
    return -1;
}

void OutputService::loadData(){
    QFile* dataFile = new QFile(this->dataFileString);
    if (dataFile->open(QIODevice::ReadWrite)){
        QDataStream dataStream(dataFile);
        int num;
        dataStream >> num;
        for (int i = 0; i < num; i++){
            AVInfo* info = new AVInfo();
            dataStream >> *info;
            this->outputs.append(info);
        }
        dataFile->close();
    }
}

void OutputService::saveData(){
    QFile* dataFile = new QFile(this->dataFileString);
    if (dataFile->open(QIODevice::ReadWrite)){
        QDataStream dataStream(dataFile);
        dataStream << this->outputs.size();
        for (int i = 0; i < this->outputs.size(); i++){
            dataStream << *(this->outputs.at(i));
        }
        dataFile->close();
    }
}
