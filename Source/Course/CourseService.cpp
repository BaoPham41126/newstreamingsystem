/***********************************************************************
 * Module:  CourseService.cpp
 * Author:  BaoPham
 * Modified: Wednesday, November 22, 2017 10:19:35 PM
 * Purpose: Implementation of the class CourseService
 ***********************************************************************/

#include "Header/Course/CourseService.h"
int CourseService::_getLectureIndex(QString id){
    for (int i = 0; i < lectures.size(); i++){
        if (lectures.at(i)->id == id){
            return i;
        }
    }
    return -1;
}

int CourseService::_getClassIndex(QString id){
    for (int i = 0; i < _classes.size(); i++){
        if (_classes.at(i)->id == id){
            return i;
        }
    }
    return -1;
}

int CourseService::_getSubjectIndex(QString id){
    for (int i = 0; i < subjects.size(); i++){
        if (subjects.at(i)->id == id){
            return i;
        }
    }
    return -1;
}

Lecture* CourseService::saveLecture(Lecture* lecture) {
    if (lecture->id == NULL){
        lecture->id = QUuid::createUuid().toString();
    }
    int index = _getLectureIndex(lecture->id);
    if (index != -1){
        lectures.removeAt(index);
    }
    lectures.append(lecture);
    this->saveData();
    return lecture;
}

Class* CourseService::saveClass(Class* _class) {
    if (_class->id == NULL){
        _class->id = QUuid::createUuid().toString();
    }
    int index = _getClassIndex(_class->id);
    if (index != -1){
        _classes.removeAt(index);
    }
    _classes.append(_class);
    this->saveData();
    return _class;
}

Subject* CourseService::saveSubject(Subject* subject) {
    if (subject->id == NULL){
        subject->id = QUuid::createUuid().toString();
    }
    int index = _getSubjectIndex(subject->id);
    if (index != -1){
        subjects.removeAt(index);
    }
    subjects.append(subject);
    this->saveData();
    return subject;
}

void CourseService::deleteLecture(QString id) {
    int index = _getLectureIndex(id);
    if (index != -1){
        lectures.removeAt(index);
        this->saveData();
    }
}

void CourseService::deleteClass(QString id) {
    int index = _getClassIndex(id);
    if (index != -1){
        _classes.removeAt(index);
        if (currentClass->id == id){
            currentClass = NULL;
        }
        QList<Lecture*> lectures = this->getLectureOfClass(id);
        for (int i = 0; i < lectures.size(); i++){
            this->deleteLecture(lectures.at(i)->id);
        }
        this->saveData();
    }
}

void CourseService::deleteSubject(QString id) {
    int index = _getSubjectIndex(id);
    if (index != -1){
        subjects.removeAt(index);
        QList<Class*> _classes = this->getClassOfSubject(id);
        for (int i = 0; i < _classes.size(); i++){
            this->deleteClass(_classes.at(i)->id);
        }
        this->saveData();
    }
}

Class* CourseService::getCurrentClass() {
    return currentClass;
}

void CourseService::setCurrentClass(Class* _class) {
    currentClass = _class;
}

QList<Subject*> CourseService::getAllSubject() {
    return this->subjects;
}

QList<Class*> CourseService::getClassOfSubject(QString subjectId){
    QList<Class*> result;
    for (int i = 0; i < _classes.size(); i++){
        if (_classes.at(i)->subjectId == subjectId){
            result.append(_classes.at(i));
        }
    }
    return result;
}

QList<Lecture*> CourseService::getLectureOfClass(QString classId){
    QList<Lecture*> result;
    for (int i = 0; i < lectures.size(); i++){
        if (lectures.at(i)->classId == classId){
            result.append(lectures.at(i));
        }
    }
    return result;
}
Lecture* CourseService::getLecture(QString id){
    int index = _getLectureIndex(id);
    if (index != -1){
        return lectures.at(index);
    } else {
        return NULL;
    }
}

Class* CourseService::getClass(QString id){
    int index = _getClassIndex(id);
    if (index != -1){
        return _classes.at(index);
    } else {
        return NULL;
    }
}

Subject* CourseService::getSubject(QString id){
    int index = _getSubjectIndex(id);
    if (index != -1){
        return subjects.at(index);
    } else {
        return NULL;
    }
}

void CourseService::loadData(){
    QFile* dataFile = new QFile(this->dataFileString);
    if (dataFile->open(QIODevice::ReadWrite)){
        QDataStream dataStream(dataFile);
        int num;
        dataStream >> num;
        for (int i = 0; i < num; i++){
            Subject* subject = new Subject();
            dataStream >> *subject;
            this->subjects.append(subject);
        }
        dataStream >> num;
        for (int i = 0; i < num; i++){
            Class* _class = new Class();
            dataStream >> *_class;
            this->_classes.append(_class);
        }
        dataStream >> num;
        for (int i = 0; i < num; i++){
            Lecture* lecture = new Lecture();
            dataStream >> *lecture;
            this->lectures.append(lecture);
        }
        QString currentClassId;
        dataStream >> currentClassId;
        if (currentClassId != "NULL"){
            this->currentClass = this->getClass(currentClassId);
        }
        dataFile->close();
    }
}

void CourseService::saveData(){
    QFile* dataFile = new QFile(this->dataFileString);
    if (dataFile->open(QIODevice::ReadWrite)){
        QDataStream dataStream(dataFile);
        dataStream << this->subjects.size();
        for (int i = 0; i < this->subjects.size(); i++){
            dataStream << *(this->subjects.at(i));
        }
        dataStream << this->_classes.size();
        for (int i = 0; i < this->_classes.size(); i++){
            dataStream << *(this->_classes.at(i));
        }
        dataStream << this->lectures.size();
        for (int i = 0; i < this->lectures.size(); i++){
            dataStream << *(this->lectures.at(i));
        }
        if (this->currentClass != NULL){
            dataStream << this->currentClass->id;
        } else {
            dataStream << "NULL";
        }
        dataFile->close();
    }
}
