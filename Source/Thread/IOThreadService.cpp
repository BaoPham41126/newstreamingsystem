/***********************************************************************
 * Module:  IOThreadService.cpp
 * Author:  BaoPham
 * Modified: Friday, November 24, 2017 1:14:39 AM
 * Purpose: Implementation of the class IOThreadService
 ***********************************************************************/

#include "Header/Thread/IOThreadService.h"

QList<IOThread*> IOThreadService::createIOThread(AVInfo* info) {
    QList<IOThread*> result;
    QList<StreamInfo*> outputOfInput;
    if (info->type == INPUT){
        outputOfInput = mapperService->getOutputOfInput(info->streams.at(0)->id);
        if (outputOfInput.size() == 0){
            return result;
        }
    }
    QList<Stream*> streams = streamService->createStreams(info);
    for (int i = 0; i < streams.size(); i++){
        IOThread* newWorker;
        Stream* stream = streams.at(i);
        AVCodecContext* codec = stream->avStream->codec;
        if (info->type == INPUT){
            InputThread* iWorker = new InputThread();
            QList<FrameBuffer*> buffers;
            for (int i = 0; i < outputOfInput.size(); i++){
                FrameBuffer* outputBuffer = bufferService->getBufferByOutputId(outputOfInput.at(i)->id);
                if (outputBuffer){
                    buffers.append(outputBuffer);
                }
            }
            iWorker->buffers = buffers;
            newWorker = iWorker;
        } else {
            OutputThread* oWorker = new OutputThread();
            oWorker->buffer = bufferService->createBuffer(stream->infoId, stream->queueSize, codec);
            newWorker = oWorker;
        }
        newWorker->stream = stream;

        QThread* thread = new QThread;
        newWorker->moveToThread(thread);
        connect(thread, SIGNAL (started()), newWorker, SLOT (run()));
        connect(newWorker, SIGNAL (finish()), thread, SLOT (quit()));
        connect(newWorker, SIGNAL (finish()), newWorker, SLOT (deleteLater()));
        connect(thread, SIGNAL (finished()), thread, SLOT (deleteLater()));
        newWorker->thread = thread;
        result.append(newWorker);
    }
    return result;
}
