#include "Header/Thread/InputThread.h"
#include "iostream"

void copy_video_frame(AVFrame **o_frame, AVFrame *i_frame, ConvertContext* convertContext) {
    AVFrame* out = av_frame_alloc();
    av_image_alloc(out->data, out->linesize, convertContext->width, convertContext->height,
            convertContext->pixelFormat, 1);
    out->width = convertContext->width;
    out->height = convertContext->height;
    out->format = i_frame->format;
    sws_scale(convertContext->context, i_frame->data, i_frame->linesize, 0,
        i_frame->height, out->data, out->linesize);
    *o_frame = out;
}

void copy_audio_frame(AVFrame **o_frame, AVFrame *i_frame) {
    AVFrame *out = av_frame_alloc();
    *out = *i_frame;
    out->data[0] = (uint8_t*) malloc(sizeof(uint8_t) * i_frame->linesize[0]);
    memcpy(out->data[0], i_frame->data[0], i_frame->linesize[0]);
    *o_frame = out;
}

AVFrame* InputThread::getReviewFrame(AVFrame* frame){
    AVFrame* frameRGB = av_frame_alloc();
    av_image_alloc(frameRGB->data, frameRGB->linesize, this->reviewPlayer->width, this->reviewPlayer->height, AV_PIX_FMT_RGB24, 1);
    frameRGB->width = this->reviewPlayer->width;
    frameRGB->height = this->reviewPlayer->height;
    frameRGB->format = AV_PIX_FMT_RGB24;
    sws_scale(this->reviewPlayer->img_convert_ctx, frame->data, frame->linesize, 0, this->reviewPlayer->sourceHeight, frameRGB->data, frameRGB->linesize);
    return frameRGB;
}

void InputThread::init(){
    if (this->stream->avStream->codec->codec_type == AVMediaType::AVMEDIA_TYPE_VIDEO) {
        for (int i = 0; i < buffers.size(); i++){
            struct SwsContext *convert_ctx = sws_getContext(
                        this->stream->avStream->codec->width,
                        this->stream->avStream->codec->height,
                        this->stream->avStream->codec->pix_fmt,
                        buffers.at(i)->codec->width,
                        buffers.at(i)->codec->height,
                        buffers.at(i)->codec->pix_fmt,
                        SWS_FAST_BILINEAR, NULL, NULL, NULL);
            ConvertContext* convertContext = new ConvertContext();
            convertContext->context = convert_ctx;
            convertContext->width = buffers.at(i)->codec->width;
            convertContext->height = buffers.at(i)->codec->height;
            convertContext->pixelFormat = buffers.at(i)->codec->pix_fmt;
            this->convertContexts.append(convertContext);
        }
    }
}

void InputThread::run(){
    AVPacket packet;
    AVPacket* i_packet = &packet;
    AVFrame* i_frame;
    AVFrame* o_frame;
    AVFrame* o_tmp_frame;
    int frameTime = 50;
    qint64 currentTime = -1;
    qint64 lastRead = -1;
    av_init_packet(i_packet);
    this->startTime = QDateTime::currentMSecsSinceEpoch();
    bool isAudio = false;
    if (this->stream->avStream->codec->codec_type == AVMediaType::AVMEDIA_TYPE_AUDIO) {
        isAudio = true;
    }
    InputStream* inputStream = qobject_cast<InputStream*>(this->stream);
    qInfo("Start input thread");
    for (int nFrame = 1; this->running; nFrame++) {
        if (inputStream->readPacket(i_packet) < 0) {
            qErrnoWarning("Error while reading packet");
            break;
        } else {
            lastRead = QDateTime::currentMSecsSinceEpoch();
        }
        currentTime = QDateTime::currentMSecsSinceEpoch();
        double currentPTS = (currentTime - startTime) / 1000.0;
        double* time = new double();
        *time = currentPTS;

        i_frame = av_frame_alloc();
        int got_frame;
        if (!isAudio) {
            //Video
            avcodec_decode_video2(this->stream->avStream->codec, i_frame, &got_frame, i_packet);
            if (got_frame) {
                if (this->reviewPlayer){
                    AVFrame* tmp = this->getReviewFrame(i_frame);
                    emit frameChange(tmp);
                }
                //Put to output buffer
                for (int i = 0; i < buffers.size(); i++){
                    copy_video_frame(&o_frame, i_frame, this->convertContexts.at(i));
                    o_frame->opaque = time;
                    this->buffers.at(i)->putFrame(o_frame);
                }
            } else {
                qErrnoWarning("Error while decoding video");
            }
            if(i_frame){
                av_free(i_frame);
                i_frame = NULL;
            }
            //FrameRate Control
            while (QDateTime::currentMSecsSinceEpoch() - lastRead < frameTime){
                QThread::msleep(10);
            }
        } else {
            //Audio
            avcodec_decode_audio4(this->stream->avStream->codec, i_frame, &got_frame, i_packet);
            if (got_frame) {
                if (this->audioReviewPlayer){
                    emit frameChange(i_frame);
                }
                //Put to output buffer
                for (int i = 0; i < buffers.size(); i++){
                   copy_audio_frame(&o_frame, i_frame);
                   uint8_t* i_samples = o_frame->data[0];
                   int i_sample_size = o_frame->linesize[0];
                   av_fifo_generic_write(buffers.at(i)->fifo, i_samples, i_sample_size, NULL);
                   if(av_fifo_size(buffers.at(i)->fifo) >= buffers.at(i)->sampleSize){
                       av_fifo_generic_read(buffers.at(i)->fifo, buffers.at(i)->o_samples, buffers.at(i)->sampleSize, NULL);
                       //fill audio sample
                       o_tmp_frame = av_frame_alloc();
                       o_tmp_frame->data[0] = (uint8_t*) malloc(sizeof(uint8_t) * buffers.at(i)->sampleSize);
                       memcpy(o_tmp_frame->data[0], buffers.at(i)->o_samples, buffers.at(i)->sampleSize);
                       o_tmp_frame->linesize[0] = buffers.at(i)->sampleSize;
                       o_tmp_frame->opaque = time;
                       this->buffers.at(i)->putFrame(o_tmp_frame);
                   }

                   //free data
                   if(o_frame->data[0]){
                       free(o_frame->data[0]);
                       o_frame->data[0] = NULL;
                   }
                   if(o_frame->opaque){
                       free(o_frame->opaque);
                       o_frame->opaque = NULL;
                   }
                   if(o_frame){
                       av_free(o_frame);
                       o_frame = NULL;
                   }
                }
            }
            else {
               qErrnoWarning("Error while decoding audio");
           }
        }
        //free memory
        if(i_packet){
            av_free_packet(i_packet);
        }
    }
    avformat_close_input(&(this->stream->format));
    for (int i = 0; i < convertContexts.size(); i++){
        sws_freeContext(convertContexts.at(i)->context);
    }
    qDeleteAll(this->convertContexts);
    this->convertContexts.clear();

    emit finish();
    this->thread->terminate();
    qInfo("Stop input complete");
}

void InputThread::stop(){
    this->running = false;
}
