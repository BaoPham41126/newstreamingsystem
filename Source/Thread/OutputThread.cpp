#include "Header/Thread/OutputThread.h"

void OutputThread::init(){

}

void OutputThread::run(){
    AVFrame* o_frame;
    AVFrame* i_frame;
    AVPacket packet;
    double* time;
    int64_t pts = 0;
    int got_packet = 0;
    int ret = 0;
    bool isAudio = false;
    if (this->stream->avStream->codec->codec_type == AVMediaType::AVMEDIA_TYPE_AUDIO) {
        isAudio = true;
    }
    OutputStream* outputStream = qobject_cast<OutputStream*>(this->stream);
    qInfo("Start output thread");
    for (int nFrame = 1; this->running; nFrame++) {
        i_frame = this->buffer->getFrame();
        if (i_frame != NULL) {
            if (isAudio) {
                //read audio sample from av_fifo
                o_frame = av_frame_alloc();
                o_frame->nb_samples = this->stream->avStream->codec->frame_size;

                //fill audio sample
                avcodec_fill_audio_frame(o_frame, this->stream->avStream->codec->channels,
                        this->stream->avStream->codec->sample_fmt, i_frame->data[0],
                        i_frame->linesize[0], 0);
            } else {
                o_frame = i_frame;
            }

            //calculate time pts
            time = (double*) i_frame->opaque;
            pts = *time / av_q2d(this->stream->avStream->time_base);

            //encode packet
            this->stream->mutex->lock();
            av_init_packet(&packet);
            packet.data = NULL;
            packet.size = 0;
            if (!isAudio){
                ret = avcodec_encode_video2(this->stream->avStream->codec, &packet, o_frame, &got_packet);
            } else {
                ret = avcodec_encode_audio2(this->stream->avStream->codec, &packet, o_frame, &got_packet);
            }
            //write to output
            if (ret >= 0 && got_packet) {
                packet.stream_index = this->stream->avStream->index;
                packet.pts = pts;
                ret = outputStream->writePacket(&packet);
//                ret = av_interleaved_write_frame(this->stream->format, &packet);
                if (ret < 0){
                    qErrnoWarning("Error while sending packet");
                }
            } else {
                qErrnoWarning("Error while encoding packet");
            }
            av_free_packet(&packet);
            this->stream->mutex->unlock();
            //free data
            if(i_frame->data[0])
                free(i_frame->data[0]);
            if(i_frame)
                av_free(i_frame);
            if(isAudio && o_frame)
                av_free(o_frame);
        } else {
            QThread::msleep(5);
        }
    }
    emit finish();
    this->thread->terminate();
    qInfo("Stop output complete");
}

void OutputThread::stop(){
    this->running = false;
    this->buffer->mutex.lock();
    this->buffer->bufferNotEmpty.wakeAll();
    this->buffer->mutex.unlock();
}
