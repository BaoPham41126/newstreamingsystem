#-------------------------------------------------
#
# Project created by QtCreator 2017-11-26T10:36:41
#
#-------------------------------------------------

QT += core network gui multimedia
greaterThan(QT_MAJOR_VERSION, 4): QT += widgets
TEMPLATE = app

QMAKE_CXXFLAGS += -I /usr/local/include -lavdevice -lavformat -lavfilter -lavcodec -lswscale -lavutil -lswresample
QMAKE_CXXFLAGS += -I /usr/include/libxml2
QMAKE_CXXFLAGS += $(shell pkg-config --cflags gtk+-2.0 gmodule-2.0 sdl)
QMAKE_CXXFLAGS += -D__STDC_CONSTANT_MACROS -c -g -I ./

INCLUDEPATH += /usr/local/include/opencv /usr/include/poppler/qt5
LIBS += -L/usr/local/lib -lavdevice  -lavformat -lavfilter -lpostproc \
    -lavcodec -ldl -lasound -lx264 -lfdk-aac -lz -lrt -lswscale -lswresample \
    -lavutil -lm  -lpthread -lcurl \
    -llog4c -lconfig -lX11 -lvpx -lv4l2 -lxml2


SOURCES += main.cpp\
    Source/Buffer/FrameBufferService.cpp \
    Source/Codec/CodecService.cpp \
    Source/Course/CourseService.cpp \
    Source/InputOutput/InputOutputMapperService.cpp \
    Source/InputOutput/InputService.cpp \
    Source/InputOutput/OutputService.cpp \
    Source/Record/RecordService.cpp \
    Source/Stream/InputStream.cpp \
    Source/Stream/OutputStream.cpp \
    Source/Stream/StreamService.cpp \
    Source/Thread/IOThreadService.cpp \
    Source/Buffer/FrameBuffer.cpp \
    Source/Youtube/Authentication.cpp \
    Source/Youtube/YoutubeService.cpp \
    Source/Thread/InputThread.cpp \
    Source/Thread/OutputThread.cpp \
    Source/View/Recording.cpp \
    Source/View/NewOutputDialog.cpp \
    Source/View/AddInputDialog.cpp \
    Source/View/Course.cpp \
    Source/View/VideoSettingDialog.cpp \
    Source/View/AudioSettingDialog.cpp \
    Source/View/Home.cpp

HEADERS  += \
    Header/Buffer/FrameBuffer.h \
    Header/Buffer/FrameBufferService.h \
    Header/Codec/CodecService.h \
    Header/Course/Class.h \
    Header/Course/CourseService.h \
    Header/Course/Lecture.h \
    Header/Course/Subject.h \
    Header/InputOutput/InputOutput.h \
    Header/InputOutput/InputOutputMapperService.h \
    Header/InputOutput/InputService.h \
    Header/InputOutput/OutputService.h \
    Header/Stream/AVStream.h \
    Header/Stream/InputStream.h \
    Header/Stream/OutputStream.h \
    Header/Stream/StreamService.h \
    Header/Thread/IOThreadService.h \
    Header/Youtube/Authentication.h \
    Header/Youtube/YoutubeService.h \
    Header/Record/RecordService.h \
    Header/Base/CallOnce.h \
    Header/Base/Singleton.h \
    Header/Thread/IOThread.h \
    Header/Thread/InputThread.h \
    Header/Thread/OutputThread.h \
    Header/InputOutput/StreamInfo.h \
    Header/InputOutput/AVInfo.h \
    Header/InputOutput/VideoStreamInfo.h \
    Header/InputOutput/AudioStreamInfo.h \
    Header/View/VideoSurface.h \
    Header/View/Recording.h \
    Header/View/NewOutputDialog.h \
    Header/View/AddInputDialog.h \
    Header/View/Course.h \
    Header/View/VideoSettingDialog.h \
    Header/View/AudioSettingDialog.h \
    Header/View/Home.h \
    Header/View/AudioSurface.h
FORMS    += \
    UI/course.ui \
    UI/home.ui \
    UI/newinputdialog.ui \
    UI/newoutputdialog.ui \
    UI/recording.ui \
    UI/VideoSettingDialog.ui \
    UI/AudioSettingDialog.ui

RESOURCES += \
    UI/image.qrc

DISTFILES += \
    UI/Image/speaker.png
