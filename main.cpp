#include <QApplication>
#include <Header/InputOutput/InputService.h>
#include <Header/InputOutput/OutputService.h>
#include <Header/InputOutput/InputOutputMapperService.h>
#include <Header/Record/RecordService.h>
#include <Header/View/Home.h>

void getYoutubeAuthentication(){
    Credential *credential = new Credential();
    Authentication* auth = new Authentication();
    if (credential->readFromFile()){
        auth->refreshToken(credential->refreshToken);
    }
}

int main(int argc, char *argv[]){
    QApplication a(argc, argv);
    avdevice_register_all();
    av_register_all();
    avformat_network_init();

    InputService* inputService = InputService::instance();
    inputService->getAvaitableVideo();
    inputService->getAvaitableAudio();
    Home home;
    home.show();
    return a.exec();
}
