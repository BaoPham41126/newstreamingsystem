#include "AudioSettingDialog.h"
#include "ui_AudioSettingDialog.h"

AudioSettingDialog::AudioSettingDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::AudioSettingDialog)
{
    ui->setupUi(this);
}

AudioSettingDialog::~AudioSettingDialog()
{
    delete ui;
}
