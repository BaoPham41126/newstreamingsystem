#ifndef AUDIOSETTINGDIALOG_H
#define AUDIOSETTINGDIALOG_H

#include <QDialog>

namespace Ui {
class AudioSettingDialog;
}

class AudioSettingDialog : public QDialog
{
    Q_OBJECT

public:
    explicit AudioSettingDialog(QWidget *parent = 0);
    ~AudioSettingDialog();

private:
    Ui::AudioSettingDialog *ui;
};

#endif // AUDIOSETTINGDIALOG_H
